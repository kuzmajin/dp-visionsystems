#pragma once

#include <string>

#include <cereal/cereal.hpp>
#include <cereal/types/string.hpp>


// Struct used to deliver commands to Camera
struct CommandData
{
	unsigned int receiver;
	std::string command;

	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(receiver, command);
	}

	static const unsigned int TYPE_ID = 5;
};