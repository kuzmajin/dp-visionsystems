#pragma once

#include <atomic>
#include <cereal/cereal.hpp>
#include <cereal/types/string.hpp>
#include <cereal/access.hpp>
#include "DataBlock.h"

// Used to ask for data
struct Request
{
	Request()
	{
		request_id = generate_id();
		if(!request_id)
			request_id = generate_id();
	}
	unsigned int request_id;

protected:
	unsigned int generate_id()
	{
		static std::atomic<std::uint32_t> uid{ 0 };
		return ++uid;
	}
};


struct HealthRequest
	: public Request
{
	HealthRequest() : Request() {}
	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(request_id);
	}

	static const unsigned int TYPE_ID = 6;
};

struct BlockRequest
	: public Request
{
	BlockRequest() : Request() {}
	BlockRequest(BlockIdentifier bi) : Request(), identifier(bi) {}

	BlockIdentifier identifier;

	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(request_id, identifier);
	}

	static const unsigned int TYPE_ID = 7;

};

struct BlockRequestQuery
	: public Request
{
	BlockRequestQuery() : Request() {}
	BlockRequestQuery(std::string q) : Request(), query(q) {}

	std::string query;

	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(request_id, query);
	}

	static const unsigned int TYPE_ID = 8;

};

