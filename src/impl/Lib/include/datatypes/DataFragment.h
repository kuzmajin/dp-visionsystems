#pragma once

#include <cereal/cereal.hpp>
#include <cereal/types/vector.hpp>
#include <vector>

typedef unsigned int FragmentIdentifier;

// Contains output of machine vision algorithms
struct DataFragment
{
	FragmentIdentifier fragment_identifier;
	unsigned int 
		source_type,
		source_device;

	//std::vector<unsigned char> data;
	std::vector<unsigned int> data;


	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(fragment_identifier , source_type , source_device , data);
	}


	static const unsigned int TYPE_ID = 3;
};
