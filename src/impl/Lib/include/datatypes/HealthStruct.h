#pragma once

#include <cereal/cereal.hpp>
#include <cereal/types/string.hpp>

// Contains health info about program
struct HealthStruct
{
	double cpu_usage;
	double ram_usage;
	unsigned int source_id;
	std::string app_name;

	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(cpu_usage, ram_usage, source_id, app_name);
	}

	static const unsigned int TYPE_ID = 4;
};