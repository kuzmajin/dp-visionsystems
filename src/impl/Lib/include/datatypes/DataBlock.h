#pragma once

#include <cereal/cereal.hpp>
#include <cereal/types/vector.hpp>
#include <vector>

typedef unsigned int BlockIdentifier;

// Contains agregated data
struct DataBlock
{
	BlockIdentifier identifier;
	bool is_defect;

	std::vector<unsigned int> data;

	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(identifier, is_defect, data);
	}

	static const unsigned int TYPE_ID = 1;

};

// Contains metadata about DataBlock
struct DatalessBlock
{
	BlockIdentifier identifier;

	template <class Archive>
	void serialize(Archive& ar)
	{
		ar(identifier);
	}

	static const unsigned int TYPE_ID = 2;

};
