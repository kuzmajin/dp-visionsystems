#pragma once

#include <vector>
#include <string>

#include "../datatypes/DataBlock.h"



// Interface used to store data
class IStorage
{
public:
	// stores data block
	virtual void store(DataBlock* data) = 0;
	// retrieves data block with identifier
	virtual DataBlock* retrieve(BlockIdentifier identifier) = 0;
	// returns array of metadata blocks
	// @date - query for date, when blocks were stored, format YYYY/MM/DD
	virtual std::vector<DatalessBlock*> retrieve_by_date(std::string date) = 0;
};