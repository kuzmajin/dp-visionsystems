#pragma once

#include "IStorage.h"

#include <filesystem>

#include <chrono>
#pragma warning(disable : 4996) //_CRT_SECURE_NO_WARNINGS
#include <ctime>


#include <cereal/archives/binary.hpp>
#include <fstream>
#include <sstream>

#include <regex>

// Stores data on local disk
class LocalStorage
	: public IStorage
{
public:
	LocalStorage(std::filesystem::path root)
		: m_root(root)
	{
		m_start = std::chrono::steady_clock::now();
	}

	virtual void store(DataBlock* data) override
	{
		auto now = std::chrono::system_clock::now();
		auto now_time_t = std::chrono::system_clock::to_time_t(now);
		auto time = std::localtime(&now_time_t);
		std::filesystem::path path = m_root;
		path /= std::to_string(time->tm_year + 1900);
		path /= std::to_string(time->tm_mon + 1);
		path /= std::to_string(time->tm_mday);

		//std::cout << "Saving datablock " << data->identifier <<  " to " << path << std::endl;
		//if (!(data->identifier % 1000))
		if (true)
		{
			auto steady_time_now = std::chrono::steady_clock::now();
			std::chrono::duration<double> diff = steady_time_now - m_start;
			std::cout << "Sec:" << std::setw(1) << diff.count() << " saving block number " << data->identifier << std::endl;
		}
		if (!CreateDirectoryRecuresive(path))
		{
			std::cout << "Couldn't create folder to save file." << std::endl;
			return;
		}
		std::stringstream ss;
		{
			cereal::BinaryOutputArchive boa(ss);
			boa(*data);
		}
		std::ofstream outFile(path /= std::to_string(data->identifier), std::ios::binary);
		if (!outFile)
		{
			std::cout << "Couldn't save file." << std::endl;
			return;
		}
		outFile << ss.str();
	}
	virtual DataBlock* retrieve(BlockIdentifier identifier) override
	{
		for (const std::filesystem::directory_entry& dir_entry :
			std::filesystem::recursive_directory_iterator(m_root))
		{
			if (dir_entry.is_regular_file())
			{
				if (dir_entry.path().filename() == std::to_string(identifier))
				{
					std::cout << dir_entry.path().string() << std::endl;
					std::ifstream inFile(dir_entry.path(), std::ios::binary);
					DataBlock* df = new DataBlock();
					{
						cereal::BinaryInputArchive bia(inFile);
						bia >> *df;
					}
					return df;
				}
			}
		}
		return nullptr;
	}
	virtual std::vector<DatalessBlock*> retrieve_by_date(std::string date) override
	{
		std::regex re("([0-9]){1,4}/([0-9]){1,2}/([0-9]){1,2}");
		if (!std::regex_match(date, re))
		{
			std::cout << "Wrong date format , use YYYY/MM/DD" << std::endl;
			return std::vector<DatalessBlock*>();
		}
		std::filesystem::path directory_path = m_root;
		std::string delimiter = "/";
		size_t pos = 0;
		std::string token;
		std::stringstream ss(date);
		while (std::getline(ss, token, '/')) {
			directory_path /= token;
		}
		std::cout << "	Retrieving all data in " + directory_path.string() << std::endl;

		std::vector<DatalessBlock*> entries{};
		if (std::filesystem::exists(directory_path) && std::filesystem::is_directory(directory_path))
		{
			for (const auto& entry : std::filesystem::directory_iterator(directory_path))
			{
				if (std::filesystem::is_regular_file(entry))
				{
					auto data = new DatalessBlock();
					data->identifier = std::stoi(entry.path().filename());
					entries.push_back(data);
				}
			}
		}
		return entries;
	}
	std::string absolute_path()
	{
		return std::filesystem::absolute(m_root).string();
	}
private:
	bool CreateDirectoryRecuresive(std::filesystem::path dir_name)
	{
		std::error_code err;
		if (!std::filesystem::create_directories(dir_name, err))
		{
			if (std::filesystem::exists(dir_name))
			{
				return true;    // the folder probably already existed
			}
			std::cout << "CreateDirectoryRecuresive: FAILED to create" + dir_name.string() + " err:" + err.message().c_str() << std::endl;
			return false;
		}
		return true;
	}






	std::chrono::steady_clock::time_point m_start;

	std::filesystem::path m_root;
};