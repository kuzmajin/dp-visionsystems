#pragma once

#include "../networking/Networking.h"
#include "IStorage.h"

#include "../datatypes/HealthStruct.h"
#include "../util/HealthMonitor.h"
#include <map>

/*
** Stores data in remote application
** Client uses IStorage interface
** Server calls IStorage on the other side
*/


// Server that accepts clients for storage
class NetworkStorageServer
{
public:
	NetworkStorageServer(int port, IStorage* storage, HealthMonitor & hm)
		:m_server(port,
			[storage, &hm]() {return std::make_shared<StorageExecutor>(storage , hm); },
			[this](std::shared_ptr<Network::Connection> connection) {clean_connections();  m_connections.push_back(connection); })
	{
		m_server.start();
	}
	~NetworkStorageServer()
	{
		m_server.stop();
	}

private:
	void clean_connections()
	{
		auto iter = m_connections.begin();;
		for (; iter != m_connections.end(); ) {
			if (!(iter->get()->is_open()))
				iter = m_connections.erase(iter);
			else
				++iter;
		}
	}


	class StorageExecutor
		: public INetworkExecutor
	{
	public:
		StorageExecutor(IStorage* storage ,	HealthMonitor& hm)
			: m_storage(storage) , m_hm(hm)
		{
			// EMPTY
		}
		virtual void on_receive(std::vector<DataBlock*> data, Network::Connection* connection, unsigned int request_id)
		{
			for (auto item : data)
			{
				m_storage->store(item);
				delete item;
			}
		}

		virtual void on_receive(std::vector<HealthRequest*> data, Network::Connection* connection, unsigned int request_id)
		{
			for (auto request : data)
			{
				connection->send_single(m_hm.get_health(), request->request_id);
			}
		}
		virtual void on_receive(std::vector<BlockRequest*> data, Network::Connection* connection, unsigned int request_id)
		{
			for (auto request : data)
			{
				auto item = m_storage->retrieve(request->identifier);
				if (item)
				{
					connection->send_single(std::vector{item}, request->request_id);
					delete request;
				}
				else
				{
					connection->send(std::vector<DataBlock*>(), request->request_id);
				}
			}
		}
		virtual void on_receive(std::vector<BlockRequestQuery*> data, Network::Connection* connection, unsigned int request_id)
		{
			for (auto request : data)
			{
				connection->send_single(std::vector{ m_storage->retrieve_by_date(request->query) }, request->request_id);
				delete request;
			}
		}


	protected:
		IStorage* m_storage;
		HealthMonitor& m_hm;
	};

	Network::Server m_server;
	std::vector<std::shared_ptr<Network::Connection>> m_connections;
};



// Connects to remote storage
class NetworkStorageClient
	: public IStorage
{
public:
	NetworkStorageClient(std::string ip_adress, int port)
		:m_connection(Network::Client::CONNECT(ip_adress, port, std::make_shared<StorageClientExecutor>(this)))
	{

	}

	virtual void store(DataBlock* data) override
	{
		m_connection->send_single(std::vector{ data });
	}

	virtual DataBlock* retrieve(BlockIdentifier identifier) override
	{
		BlockRequest block_request(identifier);
		auto response = block_promises.get_future_value(block_request.request_id);

		m_connection->send_single(block_request);
		auto items = response.get();
		if (items.size() > 0)
		{
			return std::move(items[0]);
		}
		return nullptr;
	}

	virtual std::vector<DatalessBlock*> retrieve_by_date(std::string date) override
	{
		BlockRequestQuery block_request_query{ date };
		auto response = query_promises.get_future_value(block_request_query.request_id);

		m_connection->send_single(block_request_query);
		return std::move(response.get());
	}

	// returns health data of remote storage
	virtual HealthStruct* get_health()
	{
		HealthRequest request{};
		auto response = health_promises.get_future_value(request.request_id);

		m_connection->send_single(request);
		return std::move(response.get()[0]);
	}


private:
	std::atomic<HealthStruct*>  health_handle;

	std::shared_ptr<Network::Connection> m_connection;


	std::map<unsigned int, void* > m_request_pool;
	AsyncReceive<std::vector<HealthStruct*>> health_promises;
	AsyncReceive<std::vector<DataBlock*>> block_promises;
	AsyncReceive<std::vector<DatalessBlock*>> query_promises;



	class StorageClientExecutor
		: public INetworkExecutor
	{
	public:
		StorageClientExecutor(NetworkStorageClient * nsc)
			:m_nsc(nsc)
		{

		}

		virtual void on_receive(std::vector<DataBlock*> data, Network::Connection* connection, unsigned int request_id)
		{
			m_nsc->block_promises.respond(request_id, std::move(data));
		}
		virtual void on_receive(std::vector<DatalessBlock*> data, Network::Connection* connection, unsigned int request_id)
		{
			m_nsc->query_promises.respond(request_id, std::move(data));
		}
		virtual void on_receive(std::vector<HealthStruct*> data, Network::Connection* connection, unsigned int request_id)
		{
			m_nsc->health_promises.respond(request_id, std::move(data));
		}
	private:
		NetworkStorageClient* m_nsc;
	};

};