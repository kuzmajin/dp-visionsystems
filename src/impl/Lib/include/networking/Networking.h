#pragma once
#include <SDKDDKVer.h>

#include "INetworkExecutor.h"

#include <string>
#include <asio.hpp>
#include <vector>
#include <memory>
#include <array>
#include <iostream>
#include <mutex>
#include <functional>
#include <thread>

#include <cereal/cereal.hpp>
#include <cereal/archives/binary.hpp>


using asio::ip::tcp;



//turn reference into pointer!
template<typename T>
T* ptr(T& obj) { return &obj; } 

// keep pointer
template<typename T>
T* ptr(T* obj) { return obj; } 


// Network Namespace implementing custom network protocol
namespace Network
{
	// Thrown to propagate information about closed connection
	class connection_closed_error
		: public std::runtime_error
	{
		using std::runtime_error::runtime_error;
	};

	// Size of first part of protocol, contains how many bytes of data were send over network
	static const auto SIZE_OF_SIZE = sizeof(size_t);

	// Second part of protocol, contains information about data
	struct Header
	{
		unsigned int data_type; // Which type to deserialize binary data into
		size_t item_count; // How many objects are send
		unsigned int request_id; // If message is response to Request, this value is non-zero

		template <class Archive>
		void serialize(Archive& ar)
		{
			ar(data_type , item_count , request_id);
		}
	};

	// Global namespace used to initialize components
	class NetworkLib
	{
	public:
		// Shared context between all network elements
		static asio::io_context & GET_CONTEXT()
		{
			auto & inst = GET_INSTANCE();
			return inst.get_context();
		}
		asio::io_context& get_context()
		{
			return m_io_context;
		}
	private:
		// Makes it singleton
		NetworkLib()
		{
			// EMPTY
		}

		NetworkLib(NetworkLib const&) = delete;
		void operator=(NetworkLib const&) = delete;

		static NetworkLib& GET_INSTANCE()
		{
			static NetworkLib instance;
			return instance;
		}

		asio::io_context m_io_context;
	};

	// Class used to send data between to endpoints
	class Connection 
	{
	public:
		// Before destroying, call close method
		Connection(std::shared_ptr<INetworkExecutor> executor, tcp::socket && s)
			:m_executor(executor), m_socket(std::move( s))
		{
			// Thread handles incoming data
			m_receive_thread = std::thread(&Network::Connection::receive, this);
		}
		~Connection()
		{
			m_receive_thread.join();
		}

		// Send data over network
		// @data - array of objects to send
		// @request_id - fill this value if you are responding to Request
		// ret - how many bytes were send, if == 0 then error occured
		template<typename T>
		size_t send(const std::vector<T> & data, unsigned int request_id = 0)
		{
			
			asio::streambuf buffer;
			std::ostream  ss(&buffer); // used for data serialization
			size_t size = 0;
			Header header{};
			header.data_type = ptr(data[0])->TYPE_ID;
			header.item_count = data.size();
			header.request_id = request_id;
			{ // RAII
				cereal::BinaryOutputArchive boa(ss);
				boa(header);
			}
			for (const auto& item : data)
			{ // RAII
				cereal::BinaryOutputArchive boa(ss);
				boa(*(ptr(item)));
			}
			asio::streambuf buffer2;
			std::ostream  ss2(&buffer2);
			size = buffer.size();
			ss2.write((char const*)&size, sizeof(size));
			std::vector<asio::const_buffer> vec;
			vec.push_back(buffer2.data());
			vec.push_back(buffer.data());
			//std::cout << "Sending " << SIZE_OF_SIZE << "+" << size << " bytes, request id: " << header.request_id << std::endl;
			return send_data(vec);
		}

		// Wrapper
		// ret - How many bytes were send
		template<typename T>
		size_t send_single(T data, unsigned int request_id = 0)
		{
			return send(std::vector{data}, request_id);
		}



		// Closes connection, it's save now to destroy object
		void close()
		{
			try
			{
				// Shuts down sending and receiving
				m_socket.shutdown(m_socket.shutdown_both);
				// Close socket
				m_socket.close();
			}
			catch (const asio::system_error& e)
			{
				std::cout << "Error while closing connection:\n" << e.what() << std::endl;
			}
		}

		bool is_open()
		{
			return m_socket.is_open();
		}
	private:
		// Asynchronously handles incoming data and processed them
		void receive()
		{
			try // mechanism used for handling socket closing
			{
				asio::streambuf buffer;
				while (true)
				{	
					read_n(buffer, SIZE_OF_SIZE);
					std::istream s(&buffer);
					size_t amount_to_receive;
					s.read(reinterpret_cast<char*>(&amount_to_receive), sizeof amount_to_receive);
					read_n(buffer, amount_to_receive);
					Header header;
					{ // RAII
						cereal::BinaryInputArchive bia(s);
						bia >> header;
					}
					dispatch(header, s);
				}
			}
			catch (const connection_closed_error& e )
			{
				std::cout << e.what() << std::endl;
			}
			catch (const asio::system_error& e)
			{
				std::cout << e.what() << std::endl;
			}
			
		}



		// Waits until N bytes are read from socket or socket is closed
		void read_n(asio::streambuf& container, size_t n)
		{
			container.prepare(n);
			if (container.data().size() > n)
			{
				return;
			}
			asio::error_code error;
			size_t len = asio::read(m_socket, container, asio::transfer_at_least(n-container.data().size()));

			if (error == asio::error::eof)
				throw connection_closed_error("Connection closed.");
			else if (error == asio::error::connection_reset)
				throw connection_closed_error("Connection closed by host.");
			else if (error)
			{
				std::cout << error.message() << std::endl;
				throw asio::system_error(error); // Some other error.
			}
		}

		// Calls proper actions on incoming data
		void dispatch(const Header & header , std::istream& data_stream)
		{
			switch(header.data_type)
			{
			case DataBlock::TYPE_ID:
				m_executor->on_receive(convert<DataBlock>(header.item_count, data_stream), this , header.request_id);
				break;

			case DatalessBlock::TYPE_ID:
				m_executor->on_receive(convert<DatalessBlock>(header.item_count, data_stream), this, header.request_id);
				break;

			case DataFragment ::TYPE_ID:
				m_executor->on_receive(convert<DataFragment>(header.item_count, data_stream), this, header.request_id);
				break;

			case HealthStruct::TYPE_ID:
				m_executor->on_receive(convert<HealthStruct>(header.item_count, data_stream), this, header.request_id);
				break;

			case CommandData::TYPE_ID:
				m_executor->on_receive(convert<CommandData>(header.item_count, data_stream), this, header.request_id);
				break;

			case HealthRequest::TYPE_ID:
				m_executor->on_receive(convert<HealthRequest>(header.item_count, data_stream), this, header.request_id);
				break;

			case BlockRequest::TYPE_ID:
				m_executor->on_receive(convert<BlockRequest>(header.item_count, data_stream), this, header.request_id);
			break;
			case BlockRequestQuery::TYPE_ID:
				m_executor->on_receive(convert<BlockRequestQuery>(header.item_count, data_stream), this, header.request_id);
				break;
			}
		}

		// Deserializa data
		template <typename T>
		std::vector<T*>convert(size_t count, std::istream& data_stream)
		{
			std::vector<T*> data;

			cereal::BinaryInputArchive bia(data_stream);
			for (int i = 0; i < count; ++i) 
			{
				T* item = new T();
				bia >> *item;
				data.push_back(item);
			}
			return data;
		}


		// Sends data over network
		// ret - number of bytes send, 0 if error occured
		size_t send_data(const std::vector<unsigned char> & data)
		{
			// lock is used to make sure big data are sent in proper order
			std::lock_guard<std::mutex> lock(m_send_mutex);
			try
			{
				return asio::write(m_socket, asio::buffer(data)); // function wraps write_some calls and returns only when all data has been written or error happens
			}
			catch (const asio::system_error& e)
			{
				std::cout << "Error occured while sending data:" << e.what();
				return 0;
			}
		}

		size_t send_data(std::vector<asio::const_buffer>& data)
		{
			// lock is used to make sure big data are sent in proper order
			std::lock_guard<std::mutex> lock(m_send_mutex);
			try
			{
				return asio::write(m_socket, data); // function wraps write_some calls and returns only when all data has been written or error happens
			}
			catch (const asio::system_error& e)
			{
				std::cout << "Error occured while sending data:" << e.what() << std::endl;
				return 0;
			}
		}

		tcp::socket m_socket;
		std::shared_ptr<INetworkExecutor> m_executor;
		std::mutex m_send_mutex;
		std::thread m_receive_thread;
	};


	// Creates server 
	class Server
	{
	public:
		Server(int listening_port,
			std::function<std::shared_ptr<INetworkExecutor>()> connection_executor,
			std::function<void(std::shared_ptr<Connection>)> connection_handle
		)
			: m_port(listening_port) , m_connection_executor(connection_executor), m_connection_handle(connection_handle),
			m_acceptor(NetworkLib::GET_CONTEXT())
		{
			// EMPTY
		}
		// starts server , not blocking
		void start()
		{
			should_run = true;
			m_runner = std::thread(&Network::Server::run, this);
		}

		// stops server as soon as possible
		void stop()
		{
			should_run = false;
			m_acceptor.close();
			m_runner.join();
		}

	private:
		void run()
		{
			should_run = true;
			try
			{
				auto endpoint = tcp::endpoint(tcp::v4(), m_port);
				m_acceptor.open(endpoint.protocol());
				m_acceptor.bind(endpoint);
				m_acceptor.listen();
			}
			catch (const asio::system_error& e)
			{
				std::cout << "Error while preparing server:" << e.what() << std::endl;
				throw e;
			}
			std::cout << "Accepting connections on " << m_port << std::endl;
			while (should_run)
			{
				tcp::socket socket(NetworkLib::GET_CONTEXT());
				try
				{
					m_acceptor.accept(socket);
				}
				catch (const asio::system_error& e)
				{
					std::cout << "Error when acception connections" << e.what() << std::endl;
				}
				if (!socket.is_open()) // acceptor was closed
					break;


				auto connection = std::make_shared<Connection>(m_connection_executor(), std::move(socket));
				m_connection_handle(connection);
				std::cout << "Received connection" << std::endl;
			}
		}
		std::thread m_runner;
		int m_port;
		std::atomic<bool> should_run;
		tcp::acceptor m_acceptor;


		std::function<std::shared_ptr<INetworkExecutor>()> m_connection_executor; // function provides new executor for connection
		std::function<void(std::shared_ptr<Connection>)> m_connection_handle; // action to perform with new connection
	};


	// Client class encapsulates necessary actions to create connection to server
	class Client
	{
	public:

		// Connects to server and creates connection object
		static std::shared_ptr<Connection> CONNECT(const std::string & ip_adress, int port , std::shared_ptr<INetworkExecutor> executor)
		{
			asio::ip::address_v4 addr = asio::ip::address_v4::from_string(ip_adress); // TODO - throws if bad IP 
			asio::ip::tcp::endpoint endpoint(addr, port);

			tcp::socket socket(NetworkLib::GET_CONTEXT());
			socket.connect(endpoint);
			std::cout << "Connected to " << ip_adress << ":" << port << std::endl;

			return std::make_shared<Connection>(executor, std::move(socket));
		}
	private:
		Client() {};
	};

};