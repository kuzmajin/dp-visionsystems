#pragma once

#include "../datatypes/DataTypes.h"

#include <iostream>
#include <vector>
namespace Network
{
	class Connection;
}

// Interface used to handle incoming network data asynchronously
class INetworkExecutor
{
public:
	virtual void on_receive(std::vector<DataFragment*> data, Network::Connection* connection, unsigned int request_id)
	{
		std::cout << "Received " << data.size() << " data fragments" << std::endl;
	}
	virtual void on_receive(std::vector<DataBlock*> data, Network::Connection* connection, unsigned int request_id)
	{
		std::cout << "Received " << data.size() << " data blocks" << std::endl;
	}
	virtual void on_receive(std::vector<DatalessBlock*> data, Network::Connection* connection, unsigned int request_id)
	{
		std::cout << "Received " << data.size() << " data less" << std::endl;
	}
	virtual void on_receive(std::vector<HealthStruct*> data, Network::Connection* connection, unsigned int request_id)
	{
		std::cout << "Received " << data.size() << " health structs" << std::endl;
	}
	virtual void on_receive(std::vector<CommandData*> data, Network::Connection* connection, unsigned int request_id)
	{
		std::cout << "Received " << data.size() << " command data" << std::endl;
	}
	virtual void on_receive(std::vector<HealthRequest*> data, Network::Connection* connection, unsigned int request_id)
	{
		std::cout << "Received " << data.size() << " health requests" << std::endl;
	}
	virtual void on_receive(std::vector<BlockRequest*> data, Network::Connection* connection, unsigned int request_id)
	{
		std::cout << "Received " << data.size() << " block requests" << std::endl;
	}
	virtual void on_receive(std::vector<BlockRequestQuery*> data, Network::Connection* connection, unsigned int request_id)
	{
		std::cout << "Received " << data.size() << " block requests queries" << std::endl;
	}
private:
};



#include <map>
#include <future>

// Helper class, use to exchange response asynchronously
template <class T>
class AsyncReceive
{
public:
	std::future<T> get_future_value(unsigned int request_id)
	{
		std::lock_guard<std::mutex> lock(m_lock);

		m_promise_map.insert({ request_id, std::move(std::promise<T>()) });
		return m_promise_map.find(request_id)->second.get_future();
	}

	template <class U>
	void respond(unsigned int request_id, U&& data)
	{
		std::lock_guard<std::mutex> lock(m_lock);
		auto it = m_promise_map.find(request_id);
		if (it == m_promise_map.end())
		{
			throw "Responding to request that wasn't made.";
		}
		it->second.set_value(std::forward<U>(data));
		m_promise_map.erase(it);
	}
private:
	std::map<unsigned int, std::promise<T>> m_promise_map;
	std::mutex m_lock;
};