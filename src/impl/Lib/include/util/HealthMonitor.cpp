#include "HealthMonitor.h"

#define WIN32
#include "../../../CpuMem_Monitor/cpumem_monitor.h"

// intended mischief - there will be only one instance of monitor and this approach hides system specific libs brought in by the lib
// could be replaced by using PIMPL idiom
SL::NET::CPUMemMonitor mon;


double HealthMonitor::cpu_usage()
{
	return mon.getCPUUsage().ProcessUse;
}

double HealthMonitor::mem_usage()
{
	return (double)(mon.getMemoryUsage().PhysicalProcessUsed) / (double)(mon.getMemoryUsage().PhysicalTotalAvailable);
}
