#pragma once

#include <filesystem>
#include <string>
#include <map>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <limits>
#include <stdexcept>

// exception thrown by ConfigParser
class parse_error
	: public std::runtime_error
{
	using std::runtime_error::runtime_error;
};

// Simple configuration parser, uses '=' as separator for key=value and # for comments
class ConfigParser
{
public:
	// @file_path - path to config file
	ConfigParser(const std::filesystem::path & file_path)
	{
		if (!std::filesystem::exists(file_path))
		{
			throw parse_error("File " + file_path.string() + " doesnt exist.");
		}
		if (!std::filesystem::is_regular_file(file_path))
		{
			throw parse_error("Path " + file_path.string() + " is not a file.");
		}
		std::ifstream file(file_path);
		if (!file)
		{
			throw parse_error("Couldn't open file " + file_path.string());
		}
		std::string line;
		while (std::getline(file, line))
		{
			std::istringstream is_line(line);
			if (line.rfind("#", 0) == 0) // skip comment lines
				continue;
			std::string key;
			if (std::getline(is_line, key, '='))
			{
				std::string value;
				if (std::getline(is_line, value))
					m_data[key] = value;
			}
		}
	}

	// tries to get value based on key
	// @ key - corresponding key to value
	// # throws parse_error if key isnt found or cant parse value
	template <typename T>
	T get(const std::string & key) const
	{
		T value{};
		fill(key, value);
		return value;
	}

	// wraps regular get, doesnt throw, instead returns default value
	// @key - key for value
	// @ default_value - returned if key cant be found 
	template <typename T>
	T get(const std::string& key , T default_value) const
	{
		try
		{
			return get<T>(key);
		}
		catch (const parse_error&)
		{
			return default_value;
		}
	}

	// fills value based on key, throws
	// @key - key for value
	// @value - variable to be filled
	// # throws parse_error if key isnt found or cant parse value
	void fill(const std::string & key, int & value) const
	{
		std::string data = retrieve(key);

		errno = 0;
		char* p_end{};
		long i = std::strtol(data.c_str(), &p_end, 10);
		if (i == 0 && p_end == data.c_str())
		{
			throw parse_error("Couldn't convert value " + data + "to integer");
		}
		if ((errno == ERANGE) || !range_check<int>(i))
		{
			throw parse_error("Value " + data + " out of range");
		}
		value = i;
	}

	// fills value based on key, throws
	// @key - key for value
	// @value - variable to be filled
	// # throws parse_error if key isnt found or cant parse value
	void fill(const std::string & key, double& value) const
	{
		std::string data = retrieve(key);

		errno = 0;
		char* p_end{};
		double i = std::strtod(data.c_str(), &p_end);
		if (i == 0 && p_end == data.c_str())
		{
			throw parse_error("Couldn't convert value " + data + "to floating point");
		}
		if (errno == ERANGE)
		{
			throw parse_error("Value " + data + " out of range");
		}
		value = i;
	}

	// fills value based on key, throws
	// @key - key for value
	// @value - variable to be filled
	// # throws parse_error if key isnt found
	void fill(const std::string & key, std::string& value) const
	{
		value = retrieve(key);
	}

private:

	template <typename T, typename S>
	bool range_check(const S& s) const 
	{
		return (s <= std::numeric_limits<T>::max()) && (s >= std::numeric_limits<T>::min());
	}

	// gets data from map
	std::string retrieve(const std::string & key) const
	{
		auto value = m_data.find(key);
		if (value == m_data.end())
		{
			throw parse_error("Key " + key + " not present in config file.");
		}
		return value->second;
	}

	std::map<std::string, std::string> m_data;
};