#pragma once

#include "../datatypes/HealthStruct.h"


// Monitors application , create only one instance
class HealthMonitor
{
public:
	double cpu_usage();
	double mem_usage();

	// returns HealthStruct filled with data
	HealthStruct* get_health()
	{
		HealthStruct* hs = new HealthStruct();
		hs->cpu_usage = cpu_usage();
		hs->ram_usage = mem_usage();
		hs->app_name = m_name;
		hs->source_id = m_app_id;
		return hs;
	}

	// sets name and id of application to be used in HalthStruct
	void set_data(std::string name, unsigned int app_id)
	{
		m_name = name;
		m_app_id = app_id;
	}
private:
	std::string m_name;
	unsigned int m_app_id;


};