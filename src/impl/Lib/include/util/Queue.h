#pragma once

#include <mutex>
#include <condition_variable>
#include <queue>


// Thread safe queue wrapper
template <typename T>
class Queue
{
public:
	// Gets item from queue, blocks until something is available
	T pop()
	{
		std::unique_lock<std::mutex> lock(m_queue_mutex);
		m_cond.wait(lock,
			[this]() { return !m_queue.empty(); });
		T item = m_queue.front();
		m_queue.pop();
		return item;
	}

	// Inserts item into queue
	void push(T item)
	{
		std::lock_guard<std::mutex> lock(m_queue_mutex);
		m_queue.push(item);
		m_cond.notify_one();
	}

	// Returns count of items currently in queue
	size_t size()
	{
		std::lock_guard<std::mutex> lock(m_queue_mutex);
		return m_queue.size();
	}
private:
	std::mutex m_queue_mutex;
	std::condition_variable m_cond;
	std::queue<T> m_queue;
};