#pragma once


#include "common.h"

#include "../Lib/include/util/HealthMonitor.h"


class StorageApp
{
public:
	int main(const std::vector<std::string>& arguments);
private:
	HealthMonitor m_health_monitor;
};

