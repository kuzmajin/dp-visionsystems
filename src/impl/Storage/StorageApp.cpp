#include "StorageApp.h"



#include "../Lib/include/storage/LocalStorage.h"
#include "../Lib/include/storage/NetworkStorage.h"

#include "../Lib/include/util/ConfigParser.h"

#include <regex>


struct Config
{
    int storage_server_port;
    std::string storage_root;
    int app_id;

    void fill(const ConfigParser& parser)
    {
        storage_server_port = parser.get<int>("storage_server_port");
        storage_root        = parser.get<std::string>("storage_root");
        app_id              = parser.get<int>("app_id");
    }
};




int StorageApp::main(const std::vector<std::string>& arguments)
{
	std::cout << "Hello from StorageApp" << std::endl;
    // parse config
    if (arguments.size() < 1)
    {
        std::cout << "Please provide config file as first argument" << std::endl;
        return 1;
    }
    Config config{};
    try
    {
        ConfigParser config_parser(arguments[0]);
        config.fill(config_parser);
    }
    catch (const parse_error& e)
    {
        std::cout << "Failed to parse config:\n    " << e.what() << std::endl;
        return 1;
    }
    std::cout << "Loaded config" << std::endl;
    m_health_monitor.set_data("Storage", config.app_id);

    // setup storage
	LocalStorage storage(config.storage_root);
	NetworkStorageServer storage_server(config.storage_server_port, &storage, m_health_monitor);
    std::cout << "Local storage root: " << storage.absolute_path() << std::endl;

    // Command line "interface" to interact with storage for testing purpouses
    std::string s;
    while (std::getline(std::cin, s)) {
        if (s.empty())
            continue;
        if ((s == "close" ) || (s == "quit") || (s == "stop"))
            break;
        const std::regex re("([0-9]){1,4}/([0-9]){1,2}/([0-9]){1,2}");
        if (std::regex_match(s, re))
        {
            auto data = storage.retrieve_by_date(s);
            if (data.size() == 0)
                std::cout << "  No data found." << std::endl;
            for (auto& item : data)
            {
                std::cout << "  Block id: " << item->identifier << std::endl;
                delete item;
            }
        }
        else
        {
            auto data = storage.retrieve(std::atoi(s.c_str()));
            if (data)
            {
                std::cout << "  Data id:" << data->identifier << "\n";
                std::cout << "  Data state:" << data->is_defect << "\n";
                std::cout << "  ";
                for (auto item : data->data)
                    std::cout << item;
                std::cout << std::endl;
                delete data;
            }
            else
            {
                std::cout << "  No data found for id: " << s << std::endl;
            }
        }
    }
	return 0;
}
