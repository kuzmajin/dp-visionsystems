#include "common.h"

#include "StorageApp.h"

int main(int argc, char* argv[])
{
	std::vector<std::string> arguments(argv + 1, argv + argc);

	if constexpr (DEBUG)
	{
		StorageApp app;
		return app.main(arguments);
	}
	else
	{
		try
		{
			StorageApp app;
			return app.main(arguments);
		}
		catch (const std::exception& e)
		{
			std::cout << "Unhandled exception:" << e.what() << std::endl;
		}
		catch (...)
		{
			std::cout << "Unknown unhandled exception" << std::endl;
		}
	}

	return 1;
}