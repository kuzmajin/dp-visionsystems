#include "TestAlgorithm.h"



void TestAlgorithm::run()
{
	should_run = true;
	unsigned int i = 0;
	while (i < m_generated_iterations)
	{
		std::chrono::system_clock::time_point waitUntil = std::chrono::system_clock::now() + std::chrono::milliseconds(m_sleep_ms);
		auto fragment = new DataFragment();
		fragment->fragment_identifier = ++i;
		fragment->source_type = 1;
		fragment->data.reserve(m_generated_data_size);
		for (int j = 0; j < m_generated_data_size; j++)
		{
			fragment->data.push_back(1);
		}
		std::cout << "Sending " << i << std::endl;
		m_fragment_queue.push(fragment);
		std::this_thread::sleep_until(waitUntil);
	}
}

void TestAlgorithm::stop()
{
	should_run = false;
}

void TestAlgorithm::process(std::shared_ptr<CommandData> command_data)
{
	std::cout << "Algorithm received data " << command_data->command << std::endl;
}
