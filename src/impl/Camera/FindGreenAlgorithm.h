#pragma once
#include "IAlgorithm.h"

#include "common.h"


// Algorithm implementation 
// Function: Searches for green pixels in files
class FindGreenAlgorithm :
    public IAlgorithm
{
public:
    FindGreenAlgorithm(Queue<DataFragment*>& fragment_queue , std::string data_folder)
        : IAlgorithm(fragment_queue)
    {
        if (!std::filesystem::exists(data_folder))
        {
            throw "Provided folder " + data_folder + " doesn't exist.";
        }
        if (!std::filesystem::is_directory(data_folder))
        {
            throw "Provided folder " + data_folder + " is not directory.";
        }
        std::cout << "Set to directory: " << std::filesystem::absolute(data_folder).string() << std::endl;
        m_data_folder = data_folder;
    }

    virtual void run() override;
    virtual void stop() override;
    virtual void process(std::shared_ptr<CommandData> command_data) override;
private:
    std::string m_data_folder;
    std::atomic<bool> should_run;
    std::atomic<bool> can_start; // wait for start command from client
};

