#pragma once
#include "IAlgorithm.h"

#include "common.h"

// Algorithm implementation 
// Function: Modified for purpouse of benchmarks 
class TestAlgorithm :
    public IAlgorithm
{
public:
    TestAlgorithm(Queue<DataFragment*>& fragment_queue, int generated_data_size, int generated_iterations, int sleep_ms)
        : IAlgorithm(fragment_queue), should_run(false) , 
                m_generated_data_size(generated_data_size), 
                m_generated_iterations(generated_iterations),
                m_sleep_ms(sleep_ms)
    {
        // EMPTY
    }
    virtual void run() override;
    virtual void stop() override;
    virtual void process(std::shared_ptr<CommandData> command_data) override;
private:
    bool should_run;
    int m_generated_data_size;
    int m_generated_iterations;
    int m_sleep_ms;
};