#include "CameraApp.h"

#include "TestAlgorithm.h"
#include "DataTransferProcedure.h"


#include "../Lib/include/datatypes/DataFragment.h"
#include "../Lib/include/networking/Networking.h"
#include "../Lib/include/util/ConfigParser.h"


#include "TestAlgorithm.h"
#include "FindGreenAlgorithm.h"

class CameraExecutor
    : public INetworkExecutor
{
public:
    CameraExecutor(HealthMonitor& hs, IAlgorithm * algorithm)
        : m_hm(hs), m_algorithm(algorithm)
    {

    }
    virtual void on_receive(std::vector<HealthRequest*> data, Network::Connection* connection, unsigned int request_id)
    {
        for (auto& request : data)
        {
            connection->send(std::vector{ m_hm.get_health() }, request->request_id);
            delete request;
        }
    }
    virtual void on_receive(std::vector<CommandData*> data, Network::Connection* connection, unsigned int request_id)
    {
        for (auto item : data)
        {
            m_algorithm->process(std::shared_ptr<CommandData>(item));
        }
    }

private:
    HealthMonitor& m_hm;
    IAlgorithm* m_algorithm;
};



struct Config
{
    int master_port;
    std::string master_ip;
    int app_id;
    std::string algorithm_data_folder;
    int generated_data_size;
    int generated_iterations;
    std::string algorithm;
    int sleep_ms_between_generating;

    void fill(const ConfigParser& parser)
    {
        master_port = parser.get<int>("master_port");
        master_ip = parser.get<std::string>("master_ip");
        app_id = parser.get<int>("app_id");
        algorithm_data_folder = parser.get<std::string>("algorithm_data_folder", "");
        generated_data_size = parser.get<int>("generated_data_size", 100);
        generated_iterations = parser.get<int>("generated_iterations" , 10);
        algorithm = parser.get<std::string>("algorithm", "test");
        sleep_ms_between_generating = parser.get<int>("sleep_ms_between_generating", 50);
    }
};

int CameraApp::main(const std::vector<std::string>& arguments)
{
	std::cout << "Hello from CameraApp" << std::endl;
    if (arguments.size() < 1)
    {
        std::cout << "Please provide config file as first argument" << std::endl;
        return 1;
    }
    Config config{};
    try
    {
        ConfigParser config_parser(arguments[0]);
        config.fill(config_parser);
    }
    catch (const parse_error& e)
    {
        std::cout << "Failed to parse config." << std::endl;
        std::cout << e.what();
        return 1;
    }
    std::cout << "Loaded config, app ID: " << config.app_id << std::endl;
    m_health_monitor.set_data("Camera", config.app_id);

    // select algorithm
    m_algorithm = nullptr;
    if (config.algorithm == "test")
    {
        m_algorithm = new TestAlgorithm(m_fragment_queue , config.generated_data_size, config.generated_iterations, config.sleep_ms_between_generating);
    }
    if (config.algorithm == "green")
    {
        m_algorithm = new FindGreenAlgorithm(m_fragment_queue, config.algorithm_data_folder);
    }
    if (!m_algorithm)
    {
        std::cout << "Algorithm config was not provided, supported are [test|green]" << std::endl;
        throw "Algorithm not selected";
    }

    auto connection = Network::Client::CONNECT(config.master_ip , config.master_port, std::make_shared<CameraExecutor>(m_health_monitor, m_algorithm));

    std::thread t(&IAlgorithm::run, m_algorithm);

    DataTransferProcedure<DataFragment*> transfer(
        [=]()->DataFragment* { return this->m_fragment_queue.pop(); },
        [connection, config](DataFragment* fragment) {
            fragment->source_device = config.app_id;
            connection->send(std::vector { fragment }); delete fragment;
        });

    std::thread t2(&DataTransferProcedure<DataFragment*>::run, &transfer);

    // wait for close message
    std::string s;
    while (std::getline(std::cin, s)) {
        if (s.empty())
            continue;
        if ((s == "close") || (s == "quit") || (s == "stop"))
            break;
    }

    connection->close();
    m_algorithm->stop();
    transfer.stop();
    m_fragment_queue.push(new DataFragment());
    t.join();
    t2.join();
    delete m_algorithm;
	return 0;
}
