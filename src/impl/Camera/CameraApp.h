#pragma once

#include "common.h"

#include "../Lib/include/datatypes/DataFragment.h"
#include "../Lib/include/util/Queue.h"
#include "../Lib/include/util/HealthMonitor.h"


#include "IAlgorithm.h"

// Converts main function into object world
class CameraApp
{
public:
	int main(const std::vector<std::string> & arguments);
private:
	Queue<DataFragment*> m_fragment_queue;
	IAlgorithm* m_algorithm;
	HealthMonitor m_health_monitor;
};

