#pragma once

#include <functional>



// simple procedure used to manage item exchange
template<typename T>
class DataTransferProcedure
{
public:
	DataTransferProcedure(std::function<T()> source, std::function<void(T)> destination)
		: m_source(source), m_destination(destination)
	{
		//EMPTY
	}
	// blocking fuction
	void run()
	{
		should_run = true;
		while (true)
		{
			T item = m_source();
			if (!should_run)
				break;
			m_destination(item);
		}
	}
	// stops work as soon as possible 
	void stop()
	{
		should_run = false;
	}
private:
	std::function<T()> m_source;
	std::function<void(T)> m_destination;
	bool should_run = false;
};

