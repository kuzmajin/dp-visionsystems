#pragma once

#include "../Lib/include/datatypes/DataTypes.h"
#include "../Lib/include/util/Queue.h"



// Interface for machine vision algorithms 
class IAlgorithm
{
public:
	IAlgorithm(Queue<DataFragment*>& fragment_queue)
		: m_fragment_queue(fragment_queue)
	{
		//EMPTY
	}
	// starts algorithm, is blocking
	virtual void run() = 0;
	// stops algorithm as soon as possible
	virtual void stop() = 0;
	// communicate with algorithm from outside
	virtual void process(std::shared_ptr<CommandData> command_data) = 0;
protected:
	// queue for storing processed data
	Queue<DataFragment*>& m_fragment_queue;
};

