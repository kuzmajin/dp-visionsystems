#include "FindGreenAlgorithm.h"


void FindGreenAlgorithm::run()
{
	can_start = false;
	should_run = true;
	std::cout << "Waiting for start command." << std::endl;
	while (!can_start) {} // can wait like this because of atomic variable
	std::cout << "Starting." << std::endl;
	for (const auto& entry : std::filesystem::directory_iterator(m_data_folder))
	{
		if (std::filesystem::is_regular_file(entry))
		{
			std::chrono::time_point start = std::chrono::steady_clock::now();
			std::cout << "Parsing file " << entry.path().string() << std::endl;
			std::ifstream input_file(entry.path());
			if (!input_file)
			{
				std::cout << "Couldn't open file " << entry.path().string() << std::endl;
				continue;
			}
			DataFragment* fragment = new DataFragment();
			fragment->fragment_identifier = std::stoi(entry.path().filename());
			unsigned char R, G, B;
			unsigned int iteration = 0;

			while (true)
			{
				input_file >> R >> G >> B;
				if (input_file.eof())
					break;
				if (R == 0 && G == 255 && B == 0)
				{
					fragment->data.push_back(iteration);
					std::cout << "Found:" << fragment->fragment_identifier << ":" << iteration << std::endl;
				}
				iteration++;
			}


			//std::this_thread::sleep_for(std::chrono::seconds(2));
			while (true) // implements active waiting to simulate cpu usage  
			{
				if (std::chrono::steady_clock::now() - start > std::chrono::seconds(2))
					break;
			}

			m_fragment_queue.push(fragment); // takes fragment ownership
			fragment = nullptr;
		}
	}
}

void FindGreenAlgorithm::stop()
{
	should_run = false;
}

void FindGreenAlgorithm::process(std::shared_ptr<CommandData> command_data)
{
	std::cout << "Received command " << command_data->command << std::endl;
	if (command_data->command == "start")
		can_start = true;
}
