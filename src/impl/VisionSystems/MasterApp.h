#pragma once

#include "common.h"

#include "IAgregationAlgorithm.h"
#include "DeviceHolder.h"

#include "../Lib/include/util/Queue.h"
#include "../Lib/include/datatypes/DataTypes.h"
#include "../Lib/include/util/HealthMonitor.h"

// Class wrapper for main
class MasterApp
{
public:
	int main(const std::vector<std::string>& arguments);
private:
	IAgregationAlgorithm* m_agregation;
	Queue<DataBlock*> m_block_queue;
	Queue<DataFragment*> m_fragment_queue;
	DeviceHolder m_device_holder;
	HealthMonitor m_health_monitor;
};

