#pragma once


#include "../Lib/include/util/Queue.h"
#include "../Lib/include/datatypes/DataFragment.h"
#include "../Lib/include/datatypes/DataBlock.h"

// Interface used to join DataFragments together into DataBlock
class IAgregationAlgorithm
{
public:
	IAgregationAlgorithm(Queue<DataBlock*>& block_queue, Queue<DataFragment*>& fragment_queue)
		: m_block_queue(block_queue), m_fragment_queue(fragment_queue)
	{
		// EMPTY
	}
	// Starts work, is blocking
	virtual void run() = 0;
	// Stops work as soon as possible
	virtual void stop() = 0;
protected:
	Queue<DataBlock*>& m_block_queue;
	Queue<DataFragment*>& m_fragment_queue;
};