#include "CameraSource.h"

void CameraSource::send_command(CommandData* command_data)
{
    m_connection->send_single(command_data);
}

unsigned int CameraSource::get_id()
{
    return m_id;
}

bool CameraSource::is_valid()
{
    return m_connection->is_open();
}
