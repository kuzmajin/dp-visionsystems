#pragma once
#include "IAgregationAlgorithm.h"


#include <map>
#include <atomic>

// Find green agregation, offsets pixels during agregation
class SumAgregation :
    public IAgregationAlgorithm
{
public:
    SumAgregation(Queue<DataBlock*>& block_queue, Queue<DataFragment*>& fragment_queue, unsigned int camera_count, unsigned int pixel_per_file)
        : IAgregationAlgorithm(block_queue, fragment_queue) , m_camera_count(camera_count), m_pixel_per_file(pixel_per_file)
    {
        //EMPTY
    }
    virtual void run() override;
    virtual void stop() override;
private:
    std::map<FragmentIdentifier, std::vector<DataFragment*>> m_fragment_map;
    std::atomic<bool> should_run;
    unsigned int m_camera_count;
    unsigned int m_pixel_per_file;
};

