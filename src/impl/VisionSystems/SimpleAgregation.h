#pragma once
#include "IAgregationAlgorithm.h"


// Trivial implementation of agregation, only copies DataFragments into DataBlocks
class SimpleAgregation :
    public IAgregationAlgorithm
{
public:
    SimpleAgregation(Queue<DataBlock*>& block_queue, Queue<DataFragment*>& fragment_queue)
        : IAgregationAlgorithm(block_queue, fragment_queue)
    {

    }
    virtual void run() override;
    virtual void stop() override;
};

