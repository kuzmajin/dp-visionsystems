#include "SimpleSumAgregation.h"

void SimpleSumAgregation::run()
{
	should_run = true;
	while (should_run)
	{
		auto frag = m_fragment_queue.pop();
		//std::cout << "Processing" << frag->fragment_identifier << std::endl;
		auto identifier = frag->fragment_identifier;
		auto ret = m_fragment_map.find(identifier);
		if (ret == m_fragment_map.end())
		{
			m_fragment_map[identifier] = {};
			ret = m_fragment_map.find(identifier);
		}
		ret->second.push_back(frag);
		if (ret->second.size() == m_camera_count)
		{
			std::sort(ret->second.begin(), ret->second.end(), [](const auto& lhs, const auto& rhs)
				{
					return lhs->source_device < rhs->source_device;
				});

			auto block = new DataBlock();
			block->identifier = identifier;
			for (const auto& item : ret->second)
			{
				block->data.insert(block->data.end(), item->data.begin(), item->data.end());
				delete item;
			}
			block->is_defect = (block->data.size() > 0);
			m_block_queue.push(block);
			std::cout << "Block " << block->identifier << "created." << std::endl;
			m_fragment_map.erase(identifier);

		}
	}
}

void SimpleSumAgregation::stop()
{
	should_run = false;
}
