#pragma once

#include "../Lib/include/util/HealthMonitor.h"

#include "../Lib/include/storage/NetworkStorage.h"

#include "DeviceHolder.h"

// Class that collects health data from all available apps
class HealthManager
{
public:
	HealthManager(NetworkStorageClient * storage_pointer, DeviceHolder& device_holder, HealthMonitor& master_health)
		: m_storage_pointer(storage_pointer), m_device_holder(device_holder) , m_master_health(master_health)

	{

	}

	// collects all available health data
	std::vector<HealthStruct* > get_collective_health()
	{
		std::vector<HealthStruct*> vec;
		m_device_holder.iterate([&vec] (ISourceDevice* device)
		{
				vec.push_back(device->get_health());
		});
		vec.push_back(m_storage_pointer->get_health());
		vec.push_back(m_master_health.get_health());
		std::cout << "Health data:" << std::endl;
		for (auto item : vec)
		{
			std::cout << item->app_name << std::endl;
		}
		return vec;
	}

private:
	NetworkStorageClient* m_storage_pointer;
	DeviceHolder& m_device_holder;
	HealthMonitor& m_master_health;

};

