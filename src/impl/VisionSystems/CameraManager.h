#pragma once
#include "IDeviceManager.h"

#include "../Lib/include/networking/Networking.h"

#include "DeviceHolder.h"
#include <thread>


#include "CameraSource.h"

#include "../Lib/include/datatypes/DataFragment.h"

// Implements server for Camera to join
class CameraManager :
    public IDeviceManager
{
public:
    CameraManager(DeviceHolder& device_holder, Queue<DataFragment*>& queue, unsigned int port)
        : m_device_holder(device_holder),
        m_server(port,
            [&queue, this]() {
                std::shared_ptr<CameraSourceExecutor> exec = std::make_shared<CameraSourceExecutor>(queue);
                last_made = exec;
                return exec; },
            [this](std::shared_ptr<Network::Connection> connection) {
                    auto cs = std::make_shared<CameraSource>(connection);
                    last_made->set_cs( cs.get());
                    cs->init();
                    m_device_holder.insert(cs);
                } )
    {
        m_server.start();
    }
    virtual void close() override
    {
        m_server.stop();
    }
private:
    Network::Server m_server;
    DeviceHolder& m_device_holder;
    std::shared_ptr<CameraSourceExecutor> last_made;
};

