#include "MasterApp.h"

#include "SimpleAgregation.h"
#include "SumAgregation.h"
#include "SimpleSumAgregation.h"

#include "CameraManager.h"

#include "../Lib/include/storage/LocalStorage.h"
#include "../Lib/include/storage/NetworkStorage.h"
#include "../Lib/include/util/ConfigParser.h"

#include "HealthManager.h"
#include "ClientServer.h"


struct Config
{
    int client_server_port;
    int camera_server_port;
    std::string storage_ip;
    int storage_port;
    int app_id;
    int cameras_count;
    int pixel_per_file;
    std::string algorithm;

    void fill(const ConfigParser& parser)
    {
        client_server_port = parser.get<int>("client_server_port");
        camera_server_port = parser.get<int>("camera_server_port");
        storage_ip         = parser.get<std::string>("storage_ip");
        storage_port       = parser.get<int>("storage_port");
        app_id             = parser.get<int>("app_id");
        cameras_count      = parser.get<int>("cameras_count", 1);
        pixel_per_file     = parser.get<int>("pixel_per_file" , 10);
        algorithm          = parser.get<std::string>("algorithm", "test");
    }
};



int MasterApp::main(const std::vector<std::string>& arguments)
{
    std::cout << "Hello from MasterApp" << std::endl;
    if (arguments.size() < 1)
    {
        std::cout << "Please provide config file as first argument" << std::endl;
        return 1;
    }
    Config config{};
    try
    {
        ConfigParser config_parser(arguments[0]);
        config.fill(config_parser);
    }
    catch (const parse_error& e)
    {
        std::cout << "Failed to parse config." << std::endl;
        std::cout << e.what();
        return 1;
    }
    std::cout << "Loaded config" << std::endl;
    m_health_monitor.set_data("Master", config.app_id);

    m_agregation = nullptr;
    if (config.algorithm == "test")
    {
        m_agregation = new SimpleSumAgregation(m_block_queue, m_fragment_queue, config.cameras_count);
    }
    if (config.algorithm == "green")
    {
        m_agregation = new SumAgregation(m_block_queue, m_fragment_queue, config.cameras_count, config.pixel_per_file);
    }
    if (!m_agregation)
    {
        std::cout << "Algorithm config was not provided, supported are [test|green]" << std::endl;
        throw "Algorithm not selected";
    }

    std::thread agregation_thread(&IAgregationAlgorithm::run, m_agregation);


    NetworkStorageClient* storage = new NetworkStorageClient(config.storage_ip, config.storage_port);
    HealthManager hm(storage, m_device_holder, m_health_monitor);

    CameraManager camera_manager(m_device_holder , m_fragment_queue, config.camera_server_port);

    ClientServer client_server(config.client_server_port, &m_device_holder , storage, &hm );



    std::atomic<bool> stop_storage_thread = false;
    // handle data block queue
    std::thread storage_thread(
        [storage, this, &stop_storage_thread]() {
            while (true)
            {
                auto item = m_block_queue.pop();
                if (stop_storage_thread)
                {
                    break;
                }
                storage->store(item);
                delete item;
            }
        }
    );


    std::string s;
    while (std::getline(std::cin, s)) {
        if (s.empty())
            continue;
        if ((s == "close") || (s == "quit") || (s == "stop"))
            break;
        const std::regex re("([0-9]){1,4}/([0-9]){1,2}/([0-9]){1,2}");
        if (std::regex_match(s, re))
        {
            auto data = storage->retrieve_by_date(s);
            if (data.size() == 0)
                std::cout << "  No data found." << std::endl;
            for (auto& item : data)
            {
                std::cout << "  Block id: " << item->identifier << std::endl;
                delete item;
            }
        }
        else
        {
            auto data = storage->retrieve(std::atoi(s.c_str()));
            if (data)
            {
                std::cout << "  Data id:" << data->identifier << "\n";
                std::cout << "  Data state:" << data->is_defect << "\n";
                std::cout << "  ";
                for (auto item : data->data)
                    std::cout << item;
                std::cout << std::endl;
                delete data;
            }
            else
            {
                std::cout << "  No data found for id: " << s << std::endl;
            }
        }
    }

    m_agregation->stop();
    stop_storage_thread = true;

    m_block_queue.push(new DataBlock());
    m_block_queue.push(new DataBlock());
    m_block_queue.push(new DataBlock());

    m_fragment_queue.push(new DataFragment());
    m_fragment_queue.push(new DataFragment());
    m_fragment_queue.push(new DataFragment());

    client_server.close();
    camera_manager.close();
    m_device_holder.clear();
    agregation_thread.join();
    storage_thread.join();
    std::cout << "Agregation threat joined" << std::endl;
    delete storage;
    // close storage

    return 0;
}
