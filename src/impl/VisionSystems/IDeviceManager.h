#pragma once

// Interface representing connection points for different source device
class IDeviceManager
{
public:
	virtual void close() = 0;
protected:
};

