#include "SimpleAgregation.h"

void SimpleAgregation::run()
{
	DataFragment* fragment;
	while (fragment = m_fragment_queue.pop())
	{
		std::cout << "Processing fragment " << fragment->fragment_identifier << std::endl;

		auto block = new DataBlock();
		block->identifier = fragment->fragment_identifier;
		block->is_defect = false;
		block->data = fragment->data;
		m_block_queue.push(block);
		delete fragment;
	}
}

void SimpleAgregation::stop()
{
}
