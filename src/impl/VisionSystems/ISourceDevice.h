#pragma once


#include "../Lib/include/datatypes/CommandData.h"
#include "../Lib/include/datatypes/HealthStruct.h"

// Interface representing different devices that provide DataFragments
class ISourceDevice
{
public:
	// get health data for device
	virtual HealthStruct* get_health() = 0;
	// sends command to device
	virtual void send_command(CommandData * command_data) = 0;
	// returns ID of device
	virtual unsigned int get_id() = 0;
	// Checks if device is still usable
	virtual bool is_valid() = 0;
protected:

};

