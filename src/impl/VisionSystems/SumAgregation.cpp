#include "SumAgregation.h"

#include <algorithm>

void SumAgregation::run()
{
	should_run = true;
	while (should_run)
	{
		auto frag = m_fragment_queue.pop();
		auto identifier = frag->fragment_identifier;
		auto ret = m_fragment_map.find(identifier);
		if (ret == m_fragment_map.end())
		{
			m_fragment_map[identifier] = {};
			ret = m_fragment_map.find(identifier);
		}
		ret->second.push_back(frag);
		if (ret->second.size() == m_camera_count)
		{
			std::sort(ret->second.begin(), ret->second.end(), [](const auto& lhs, const auto& rhs)
				{
					return lhs->source_device < rhs->source_device;
				});

			auto block = new DataBlock();
			block->identifier = identifier;
			int i = 0;
			for (const auto& item : ret->second)
			{
				for (const auto pixel_pos : item->data)
					block->data.push_back(pixel_pos + i * m_pixel_per_file);
				++i;
				delete item;
			}
			block->is_defect = (block->data.size() > 0);
			m_block_queue.push(block);
			m_fragment_map.erase(identifier);

		}
	}
}

void SumAgregation::stop()
{
	should_run = false;

}
