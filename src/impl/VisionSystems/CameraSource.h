#pragma once
#include "ISourceDevice.h"



#include "../Lib/include/networking/Networking.h"


#include <memory>

#include "../Lib/include/util/Queue.h"

// Represents Camera instance
class CameraSource :
    public ISourceDevice
{
public:
    CameraSource(std::shared_ptr<Network::Connection> connection)
        : m_connection(connection)
    {
    }
    void init()
    {
        auto hs = get_health();
        m_id = hs->source_id;
        std::cout << hs->app_name << " " << hs->source_id << " connected" << std::endl;
        delete hs;
    }



    virtual HealthStruct* get_health()
    {
        HealthRequest request{};
        auto response = health_promises.get_future_value(request.request_id);

        m_connection->send_single(request);
        return std::move(response.get()[0]);
    }

    virtual void send_command(CommandData* command_data) override;
    virtual unsigned int get_id() override;

    virtual bool is_valid() override;


private:
    unsigned int m_id;

    std::shared_ptr<Network::Connection> m_connection;
    std::atomic < HealthStruct*>  m_health_value;
    AsyncReceive<std::vector<HealthStruct*>> health_promises;
    friend class CameraSourceExecutor;



};

class CameraSourceExecutor
    : public INetworkExecutor
{
public:
    CameraSourceExecutor(Queue<DataFragment*>& queue)
        : m_fragment_queue(queue)
    {
    }
    void set_cs(CameraSource* cs)
    {
        m_cs = cs;
    }


    virtual void on_receive(std::vector<DataFragment*> data, Network::Connection* connection, unsigned int request_id) override
    {
        //std::cout << "Received fragments " << data.size() << std::endl;
        for (auto& item : data)
        {
            m_fragment_queue.push(item);
        }
    }
    virtual void on_receive(std::vector<HealthStruct*> data, Network::Connection* connection, unsigned int request_id) override
    {
        m_cs->health_promises.respond(request_id, std::move(data));
        /*if (data.size() == 1)
        {
            m_cs->m_health_value = data[0];
        }
        else
        {
            throw "Error, received != 1 health info.";
        }*/
    }


private:
    Queue<DataFragment*>& m_fragment_queue;
    CameraSource* m_cs;
};
