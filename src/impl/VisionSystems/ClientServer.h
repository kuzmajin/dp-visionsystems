#pragma once


#include "../Lib/include/networking/Networking.h"


#include "DeviceHolder.h"
#include "../Lib/include/storage/IStorage.h"
#include "HealthManager.h"

// Server used by client to connect to Master
class ClientServer
{
public:
	ClientServer(int client_port, DeviceHolder * dh, IStorage * storage , HealthManager * hm)
		: m_server(client_port,
			[this]() {return std::make_shared<Executor>(this); },
			[this](std::shared_ptr<Network::Connection> connection) {clean_connections() , m_connections.push_back(connection); }),
		m_dh(dh), m_storage(storage), m_hm(hm)
	{
		m_server.start();
	}
	void close()
	{
		m_server.stop();
	}

private:
	void clean_connections()
	{
		auto iter = m_connections.begin();;
		for (; iter != m_connections.end(); ) {
			if (!(iter->get()->is_open()))
				iter = m_connections.erase(iter);
			else
				++iter;
		}
	}


	class Executor
		: public INetworkExecutor
	{
	public:
		Executor(ClientServer* cs)
			: m_cs(cs)
		{

		}


		virtual void on_receive(std::vector<CommandData*> data, Network::Connection* connection, unsigned int request_id)
		{
			for (auto& request : data)
			{
				m_cs->m_dh->iterate(
					[request](ISourceDevice* device) {
						if (device->get_id() == request->receiver)
						{
							device->send_command(request);
						}
					}
				);
			}
		}
		virtual void on_receive(std::vector<HealthRequest*> data, Network::Connection* connection, unsigned int request_id)
		{
			for (auto& request : data)
			{
				connection->send(m_cs->m_hm->get_collective_health(), request->request_id);
				delete request;
			}
		}
		virtual void on_receive(std::vector<BlockRequest*> data, Network::Connection* connection, unsigned int request_id)
		{
			for (auto& request : data)
			{
				auto item = m_cs->m_storage->retrieve(request->identifier);
				if (item)
				{
					connection->send_single(item, request->request_id);
					delete request;
				}
				else
				{
					connection->send(std::vector<DataBlock*>(), request->request_id);
				}
			}
		}
		virtual void on_receive(std::vector<BlockRequestQuery*> data, Network::Connection* connection, unsigned int request_id)
		{
			for (auto& request : data)
			{
				connection->send(m_cs->m_storage->retrieve_by_date(request->query), request->request_id);
				delete request;
			}
		}
	private:
		ClientServer* m_cs;
	};

	Network::Server m_server;

	std::vector<std::shared_ptr<Network::Connection>> m_connections;

	DeviceHolder* m_dh;
	IStorage* m_storage;
	HealthManager* m_hm;

	friend class Executor;
};

