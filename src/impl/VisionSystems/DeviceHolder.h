#pragma once

#include "ISourceDevice.h"
#include <vector>
#include <mutex>
#include <functional>

// Holds all devices
class DeviceHolder
{
public:
	DeviceHolder()
	{
		// EMPTY
	}

	// store device
	void insert(std::shared_ptr<ISourceDevice> item)
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		clean_up();
		m_devices.push_back(item);
	}

	// drops all devices
	void clear()
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		m_devices.clear();
	}

	// iterates over all devices and calls func with device as argument
	void iterate(std::function<void(ISourceDevice*)> func)
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		clean_up();
		auto iter = m_devices.begin();;
		for (; iter != m_devices.end(); ++iter)
		{
			func(iter->get());
		}
	}
private:
	// removes hanging devices
	void clean_up()
	{
		auto iter = m_devices.begin();;
		for (; iter != m_devices.end(); ) 
		{
			if (!(iter->get()->is_valid()))
			{
				iter = m_devices.erase(iter);
			}
			else
			{
				++iter;
			}
		}
	}



	std::vector<std::shared_ptr<ISourceDevice>> m_devices;
	std::mutex m_mutex;
};