#pragma once
#include "IAgregationAlgorithm.h"

#include <map>
#include <atomic>

// Agregation algorithm joins together N fragments with no further processing
class SimpleSumAgregation :
    public IAgregationAlgorithm
{
public:
    SimpleSumAgregation(Queue<DataBlock*>& block_queue, Queue<DataFragment*>& fragment_queue, unsigned int camera_count)
        : IAgregationAlgorithm(block_queue, fragment_queue), m_camera_count(camera_count)
    {
        //EMPTY
    }
    virtual void run() override;
    virtual void stop() override;
private:
    std::map<FragmentIdentifier, std::vector<DataFragment*>> m_fragment_map;
    std::atomic<bool> should_run;
    unsigned int m_camera_count;
};

