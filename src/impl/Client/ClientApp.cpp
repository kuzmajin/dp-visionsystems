#include "ClientApp.h"

#include <regex>
#include "ClientInterface.h"


#include "../Lib/include/util/ConfigParser.h"


struct Config
{
    int server_port;
    std::string server_ip;

    void fill(const ConfigParser& parser)
    {
        server_port = parser.get<int>("server_port");
        server_ip = parser.get<std::string>("server_ip");
    }
};



int ClientApp::main(const std::vector<std::string>& arguments)
{
	std::cout << "Hello from ClientApp" << std::endl;
    if (arguments.size() < 1)
    {
        std::cout << "Please provide config file as first argument" << std::endl;
        return 1;
    }
    Config config{};
    try
    {
        ConfigParser config_parser(arguments[0]);
        config.fill(config_parser);
    }
    catch (const parse_error& e)
    {
        std::cout << "Failed to parse config." << std::endl;
        std::cout << e.what();
        return 1;
    }
    std::cout << "Loaded config" << std::endl;

    ClientInterface ci(config.server_ip, config.server_port);


    std::string s;
    while (std::getline(std::cin, s)) {
        if (s.empty())
            continue;
        if ((s == "close") || (s == "quit") || (s == "stop"))
            break;
        if (s == "health")
        {
            auto data = ci.request_health_update();
            if (data.size() == 0)
                std::cout << "  No health data provided." << std::endl;
            for (auto& item : data)
            {
                std::cout << item->app_name << " " << item->source_id << " CPU: " << item->cpu_usage  << "%" << std::endl;
                delete item;
            }
            continue;
        }
        const std::regex regex_start("start ([0-9])*");
        if (std::regex_match(s, regex_start))
        {
            std::string id = s.substr( s.find(" ") + 1);
            std::cout << "Sending start to:" << id << std::endl;
            ci.send_command("start", std::atoi(id.c_str()));
            continue;
        }
        const std::regex re("([0-9]){1,4}/([0-9]){1,2}/([0-9]){1,2}");
        if (std::regex_match(s, re))
        {
            auto data = ci.block_query(s);
            if (data.size() == 0)
                std::cout << "  No data found." << std::endl;
            for (auto& item : data)
            {
                std::cout << "  Block id: " << item->identifier << std::endl;
                delete item;
            }
            continue;
        }
        const std::regex regex_blockidentifier("[0-9]*");
        if(std::regex_match(s, regex_blockidentifier))
        {
            auto data = ci.request_data_block(std::atoi(s.c_str()));
            if (data.size() == 0)
                std::cout << "  No data found." << std::endl;
            for (auto& item : data)
            {
                std::cout << "  Data id:" << item->identifier << "\n";
                std::cout << "  Data state:" << item->is_defect << "\n";
                std::cout << "  ";
                for (auto item : item->data)
                    std::cout << item << " ";
                std::cout << std::endl;
                delete item;
            }
            continue;
        }
    }


	return 0;
}
