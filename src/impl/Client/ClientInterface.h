#pragma once

#include "../Lib/include/networking/Networking.h"

#include <atomic>

// Class that communicates with master
class ClientInterface
{
public:
	ClientInterface(std::string server_ip, int port)
		: m_connection(Network::Client::CONNECT(
			server_ip , port , std::make_shared<ClientExecutor>(this)
		))
	{

	}
	~ClientInterface()
	{
		m_connection->close();
	}

	// asks master for health status of connected apps
	std::vector<HealthStruct*> request_health_update()
	{
		HealthRequest request{};
		auto response = health_promises.get_future_value(request.request_id);

		m_connection->send_single(request);
		return std::move(response.get());
	}

	// asks storage for data block with identifier
	// @ block_identifier - block identifier, should be obtained from DatalessBlock
	std::vector<DataBlock*> request_data_block(BlockIdentifier block_identifier)
	{
		BlockRequest block_request( block_identifier );
		auto response = block_promises.get_future_value(block_request.request_id);

		m_connection->send_single(block_request);
		return std::move(response.get());
	}

	// asks for dataless blocks with query
	// @query - string in formar YYYY/MM/DD
	std::vector<DatalessBlock*> block_query(std::string query)
	{
		BlockRequestQuery block_request_query{ query };
		auto response = query_promises.get_future_value(block_request_query.request_id);

		m_connection->send_single(block_request_query);
		return std::move(response.get());
	}

	// sends command to Camera instance
	// @command - string to be sent
	// @receiver - id of instance that is supposed to receive command
	void send_command(std::string command, unsigned int receiver)
	{
		CommandData command_data;
		command_data.command = command;
		command_data.receiver = receiver;
		m_connection->send(std::vector{ command_data });
	}


private:
	class ClientExecutor
		: public INetworkExecutor
	{
	public:
		ClientExecutor(ClientInterface * ci)
			:m_ci(ci)
		{

		}
		virtual void on_receive(std::vector<DataBlock*> data, Network::Connection* connection, unsigned int request_id)
		{
			m_ci->block_promises.respond(request_id, std::move(data));
		}
		virtual void on_receive(std::vector<DatalessBlock*> data, Network::Connection* connection, unsigned int request_id)
		{
			m_ci->query_promises.respond(request_id, std::move(data));
		}
		virtual void on_receive(std::vector<HealthStruct*> data, Network::Connection* connection, unsigned int request_id)
		{
			m_ci->health_promises.respond(request_id, std::move(data));
		}
	private:
		ClientInterface* m_ci;
	};
	std::shared_ptr<Network::Connection> m_connection;

	AsyncReceive<std::vector<HealthStruct*>> health_promises;
	AsyncReceive<std::vector<DataBlock*>> block_promises;
	AsyncReceive<std::vector<DatalessBlock*>> query_promises;
	 
};