#include <vector>
#include <string>

#include <filesystem>
#include <iostream>
#include <fstream>

#include <random>
#include <chrono>

#include "../Lib/include/util/ConfigParser.h"

/*
** Simple app used to generate data for find green agorithm
*/


int main(int argc, char* argv[])
{
	std::vector<std::string> arguments(argv + 1, argv + argc);
	if (arguments.size() < 1)
	{
		std::cout << "Please provide config file as first argument" << std::endl;
		return 1;
	}
	ConfigParser config_parser(arguments[0]);

	int files_count = config_parser.get<int>("files_count");
	int folders_count = config_parser.get<int>("folders_count");
	int pixels_per_file = config_parser.get<int>("pixels_per_file");
	int green_in_file_chance = config_parser.get<int>("green_in_file_chance");
	int green_in_file_count = config_parser.get<int>("green_in_file_count");

	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::mt19937 rng_gen;
	rng_gen.seed(seed);

	std::filesystem::create_directory("test_data");
	std::filesystem::path  path = std::filesystem::current_path();

	std::filesystem::path data_path = ( path /= "test_data");

	std::cout << "Prep done." << std::endl;
	for (int i = 1; i <= folders_count; i++)
	{
		std::filesystem::create_directory(data_path / std::to_string(i));
	}

	std::cout << "Green pixels:" << std::endl;
	for (int i = 1; i <= files_count; ++i)
	{
		int pixel_index = 0;
		unsigned char R, G, B;

		bool is_green = (rng_gen() % 100) < green_in_file_chance;
		unsigned int greens = 0;
		unsigned int green_pixel_chance = pixels_per_file * folders_count;
		for (int j = 1; j <= folders_count; j++)
		{
			auto file_path = (data_path / std::to_string(j)) / std::to_string(i);
			std::ofstream file(file_path, std::ios_base::binary);
			if (!file)
			{
				std::cout << "Couldn't open file." << file_path << std::endl;
				return 1;
			}

			for (int k = 0; k < pixels_per_file; k++)
			{
				if (is_green && (greens < green_in_file_count) && ((rng_gen() % green_pixel_chance <  green_in_file_count)))
				{
					R = 0;
					G = 255;
					B = 0;
					++greens;
					std::cout << "    File:" <<  i << " index:" << pixel_index << std::endl;
				}
				else
				{
					R = 0;
					G = 0;
					B = 0;
				}


				file << R << G << B;
				pixel_index++;
			}
		}
	}

	std::cout << "Generation successful." << std::endl;
	return 0;
}