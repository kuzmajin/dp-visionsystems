\part{Praktická část}

\chapter{Návrh řešení}
    Návrh je rozdělen do několika částí. Každá část se zaměřuje na jeden problém, který ovlivňuje implementaci celého řešení.
    
    \section{Architektura}
        Jako základ pro návrh architektury je použit koncept klient-server. Definice jednotlivých částí je následující:
        \begin{description}
            \item[Master] Stará se o agregaci dat a celkové řízení řešení.
            \item[Klient] Posílá požadavky na data a poskytuje přístup z venčí.
            \item[Úložiště] Poskytuje perzistentní uložení dat.
            \item[Kamera] Přestavuje zdroj zpracovaných dat a zprostředkovává prostředí pro běh algoritmu počítačového vidění.
        \end{description}


        Každá část je zodpovědná za svůj vlastní chod a s ostatními částmi komunikuje přes síť. Tento vztah je znázorněn na diagramu~\ref{fig:network_diagram}. Diagram odpovídá originálnímu diagramu z teoretické části, ale je doplněn o vztah Klienta a Úložiště. Jedná se o přidanou hodnotu s nulovými náklady. Klient může využít stejný kód, jako je dostupný pro Master pro připojení. Příklad využití může být, že si uživatel chce prohlížet historická data a nemusí spouštět zbylé 2 části řešení. 
    
        \begin{figure}[ht]\centering
            \includegraphics[width=1\textwidth]{img/network-diagram.pdf}
            \caption[Reprezentace vztahů klient-server]{Reprezentace vztahů klient-server}\label{fig:network_diagram}
        \end{figure}

    
    \section{Reprezentace dat}
        Řešení je založeno na práci s daty, která nejsou předem definovaná. Formát dat by měl co nejlépe odpovídat výstupům algoritmů, které zpracovávají obraz. Tyto algoritmy mohou ale produkovat jakákoliv data. Například:
        \begin{itemize}
            \item Pozice defektu a obrázek defektu.
            \item Celý zaznamenaný obraz kamery.
            \item Řetězce znaků z OCR (tištěné ID produktu).
            \item Úhel mezi stranami produktu.
            \item Rozměry produktu.
        \end{itemize}
        Je možné, že bude zapotřebí agregovat výstup z dvou různých zdrojů. Jediný unifikační prvek v~takovém případě je použití proměnné typu boolean reprezentující nalezení defektu. Tím se ale ztratí dodatečné informace, například přiložený obrázek defektu, sloužící jako důkaz.

        Jako příklad lze uvést tabuli skla. U takového produktu lze hledat defekty na povrchu, kontrolovat správnou velikost a číst sériové číslo. V tomto případě jsou generovány 3 různé výstupy, které musí být agregovány. Vzhledem k okolnostem bylo zvoleno nejobecnější možné řešení.

        Obecné řešení je založené na odstranění typů na úrovni programovacího jazyka. Pro výstup z~algoritmů byla zvolena struktura \verb/DataFragment/ s předpisem~\ref{list:datafragment} a pro výstup z agregace struktura \verb/DataBlock/ s předpisem~\ref{list:datablock}. K uložení dat jsou použita pole znaků, neboli binárních dat. Algoritmy nyní mohou mít na výstupu jakákoliv data, pokud jsou převedena do binární či textové podoby. Jednou takovou textovou podobou může být formát JSON. Agregační část je nyní zodpovědná za správné převedení dat.

\begin{lstlisting}[caption={~DataFragment struktura},label=list:datafragment,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
struct DataFragment
{
	FragmentIdentifier fragment_identifier;
	unsigned int 
		source_type,
		source_device;
	std::vector<unsigned char> data;
};
\end{lstlisting}
        
\begin{lstlisting}[caption={~DataBlock struktura},label=list:datablock,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
struct DataBlock
{
	BlockIdentifier identifier;
	bool is_defect;
	std::vector<unsigned char> data;
};
\end{lstlisting}	

        Takto obecné řešení ale znamená, že na úrovni zdrojového kódu se ztrácí informace o typech a přichází se tak o metody kontroly jako je typová kontrola na straně kompilátoru. Pokud je zvolená reprezentace nevyhovující, je možné řešení modifikovat dvěma způsoby.

        \begin{description}
            \item[Konkretizace] Pro práci s pouze jedním typem, lze nahradit typ proměnné \verb/data/ konkrétním typem, který nejlépe vyhovuje řešení. Tím by mohlo být například pole pixelů. Postup lze silně doporučit jakmile bude řešení použito v konkrétní doméně. Především pro strukturu \verb/DataBlock/.
            \item[Polymorfismus] Polymorfismus umožňuje různým strukturám se stejným chováním, aby byly použity ve stejném kódu. To ale znamená, že se na ven tváří jako jedna stejná struktura. Pokud má být zachován typ, je potřeba použít techniku \verb/Double Dispatch/. V opačném případě bude program muset provádět přetypování stejně jako tomu je u aktuálního návrhu. Použitím \verb/Double Dispatch/\footnote{Double Dispatch je technika, která během běhu programu volá funkci v závislosti na typech obou objektů zahrnutých do volání.} se ale výrazně zkomplikuje práce s daty kvůli nutnosti přidat extra logiku a zhorší se tím i postup případné integrace jiných programovacích jazyků. 
        \end{description}

    \section{Bezpečnost}
        V rámci sběru požadavků nebyly definovány žádné požadavky na bezpečnost. Řešení nepracuje s žádnými citlivými daty, i tak je vhodné problematiku nezanedbat. Pro navrženou aplikaci lze vést polemiku na úrovní lokální a síťové.

        Existující řešení jsou uzavřené systémy, které se používají pouze k běhu vision systému. Lze předpokládat, že v reálném nasazení je lokální stroj plně pod správou technického personálu. Pokud někdo má zájem na škodném jednání, je fyzický útok velmi snadnou variantou a jakákoliv ochrana na úrovni aplikace se stává neúčinnou. Pozornost si zaslouží uložení dat, především jejich integrita. Pokud je potřeba zaručit dlouhodobé a bezporuchové uložení, je vhodné zvolit dedikovaná řešení, které se problematikou zabývá.

        Použitím protokolu TCP pro síťovou komunikaci je k dispozici mechanismus kontrolní sumy pro zaručení integrity a pořadové číslo, aby se předešlo ztrátě paketů. Samotná komunikace není nijak šifrována. Pro navržené řešení je možné, že Úložiště bude řešeno například ukládáním dat ve vzdáleném datovém centru. V takovém případě je potřeba zvážit, zda je potřeba data šifrovat.


    \section{Protokol pro síťovou komunikaci}
        Protokol TCP  nerozumí typům, které jsou použity ve zdrojovém kódu. Hlavním cílem je předat dříve definované datové struktury mezi jednotlivými částmi. Dále se budou posílat informace o~měření výkonu a Klient může posílat data do části Kamera. 

        Je možné vytvořit jedno spojení mezi každým párem pro každý typ dat. Takové řešení je ale neefektivní a implementace bude nepřehledná. Proto je vhodnější nadefinovat vlastní síťový protokol pracující nad TCP. Pro návrh vlastního protokolu se nejdříve zadefinuje, jaká data budou posílána a poté bude zvolena vhodná podoba.

        Protokol je orientován na předávání datových struktur. Proto je zaveden koncept požadavků (Request). Ty příjemci říkají, jaká data má odeslat.

        \begin{description}
            \item[DataBlock] Agregovaná data.
            \item[DatalessBlock] Pouze metadata z DataBlock.
            \item[DataFragment] Zpracovaná data určená k agregaci.
            \item[HealthData] Informace o aktuální vytíženosti.
            \item[CommandData] Data určená k obsluze Kamera části.
            \item[Request] Rozhraní obsahující identifikátor požadavku.
            \item[HealthRequest] Požadavek na všechna dostupná HealthData.
            \item[BlockRequets] Požadavek na DataBlock.
            \item[BlockRequestQuery] Požadavek na DatalessBlock obsahující dotaz.
        \end{description}

        Struktury jako je \verb/DatalessBlock/ dává smysl posílat jako pole objektů. Pro zjednodušení bude uvažováno že každá výměna dat reprezentuje pole objektů. Dále bude každé pole obsahovat data pouze jednoho typu. Nově zmíněná struktura \verb/DatalessBlock/ má umožnit zpřístupnit informace o tom, jaké \verb/DataBlock/ jsou uložené v Úložišti bez toho, aniž by bylo potřeba posílat i objemnou datovou část. \verb/DatalessBlock/ si lze vyžádat posláním \verb/BlockRequestQuery/. Tímto je shrnuto, co se bude posílat.
        
        Zvolená knihovna \verb/Cereal/ umožňuje serializaci dat do binární podoby. Při této serializaci se ale ztrácí informace o typu. Proto je definována struktura \verb/Header/ s předpisem~\ref{list:network_header}, která bude informaci uchovávat. Následně je uložena velikost celého balíčku. Výsledná podoba celého paketu je znázorněna na obrázku \ref{fig:network_packet}


\begin{lstlisting}[caption={~Header struktura},label=list:network_header,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
struct Header
{
	unsigned int data_type;
	size_t item_count;
};
\end{lstlisting}	

        \begin{figure}[ht]\centering
            \includegraphics[width=0.4\textwidth]{img/network-packet.pdf}
            \caption[Datový formát paketu]{Datový formát paketu pro navrhovaný protokol.}\label{fig:network_packet}
        \end{figure}

        \paragraph*{Práce s paketem bude probíhat následovně:}
        \begin{enumerate}
            \item Vytvoření hlavičky.
            \item Serializace hlavičky a dat.
            \item Výpočet velikosti serializované hlavičky a dat
            \item Odeslání velikosti + binárních dat.
        \end{enumerate}

        \paragraph*{Přijetí paketu:}
        \begin{enumerate}
            \item Přijetí velikosti.
            \item Přijetí množství dat uvedených ve velikosti.
            \item Deserializace hlavičky.
            \item Zpracování zbytku dat podle typu uvedeného v hlavičce.
        \end{enumerate}
        Posílání velkých dat není problém, jelikož TCP zajišťuje správné pořadí paketů a příjemce ví, kolik dat má přijmout. Tím je vyřešen problém s fragmentací, která nastane, když jsou data příliš velká, aby se vešla do jednoho TCP paketu. 

    \section{Měření výkonu}
        Pro měření výkonu byla vybrána technika průběžného monitorování systémových prostředků. Každá část řešení kromě Klient bude schopna poslat data o tom, jak moc běh aplikace zatěžuje procesor. Jelikož Klient komunikuje pouze s Master, využije možnost posílat pole objektů přes síť a Klient obdrží data, která Master posbírá z přidružených aplikací. 




\chapter{Implementace}
    K implementaci je použit jazyk C++ doplněn o dříve zmíněné knihovny. Samotná implementace je rozdělena podle jednotlivých částí řešení. Jelikož je cílem vytvořit 4 různé aplikace, je zde část kódu, která je sdílena. Text je doplněn o ukázky kódu, které nemusí odpovídat skutečné implementaci. Kód byl pozměněn pro lepší čitelnost a kompaktnost. Pořadí jednotlivých podkapitol bylo zvoleno na základě závislosti vyplývajících z návrhu. 
    \section{Sdílená část}
    Prvky, které lze použít mezi více částmi byly vyčleněny do samostatného celku podle principu DRY. Aby bylo možné co nejvíce kódu sdílet, jsou použity templaty pro tvorbu generického kódu. Proto byla pro sdílenou část zvolena forma \verb/hlavičkových souborů/ místo samostatné knihovny. V předchozích kapitolách byly již zmíněny datové struktury, proto jsou zde přeskočeny.

    První implementovanou třídou je \verb/ConfigParser/, viz kód~\ref{list:configparser}. Pro práci se sítí je potřeba mít k dispozici IP adresu a port. Dále bude potřeba konfigurovat jednotlivé instance Kamera. Aby nebylo potřeba kompilovat aplikace při změně těchto parametrů, lze použít několik technik. Například registry, čtení konfigurace ze souboru nebo argumenty příkazové řádky. Byla zvolena kombinace čtení ze souboru a argumentů příkazové řádky. Z příkazové řádky je přečtena adresa souboru a ten pak obsahuje hodnoty jednotlivých proměnných. Samotný formát souboru používá řádky k oddělení proměnných a znaménko = k určení klíče a hodnoty. Příklad formátu:
 \begin{verbatim}
master_port=1000
master_ip=127.0.0.1
  \end{verbatim}
    
\begin{lstlisting}[caption={~Předpis Třídy ConfigParser},label=list:configparser,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
class ConfigParser
{
public:
    ConfigParser(const std::filesystem::path & file_path)
    template <typename T>
    T get(const std::string & key) const;
private:
    std::map<std::string, std::string> m_data;
};
\end{lstlisting}

        Další vyčleněnou částí je práce se sítí. Jmenný prostor \verb/Network/ znázorněný v předpisu kódu~\ref{list:network_namespace} zabaluje volání knihoven asio a cereal. Implementace odpovídá dříve navrženému protokolu. Protože je komunikace čistě asynchronní, je přidáno rozhraní \verb/INetworkExecutor/. Ze soketu smí číst pouze jedno dedikované vlákno, to se stará o deserializaci dat. Jakmile jsou data deserializována a jejich typ je zjištěn, zavolá se odpovídající metoda v \verb/INetworkExecutor/. Pokud aplikace chce obsloužit daný typ objektu, implementuje třídu která dědí rozhraní, přetíží metodu a data může zpracovat. Jinak se s daty nic nestane. 

        Aby bylo možné obsloužit \verb/Request/ a vrátit data správnému vláknu, používá se identifikátor. Ten je členskou proměnnou struktury \verb/Request/ a jejích potomků. V odpovědi na \verb/Request/ se uvede identifikátor zkopíruje do hlavičky a příjemce ví, komu data předat. Pro předání se použije knihovna \verb/future/ a třída \verb/std::promise/. Takto definované postupy umožní použití jednotné implementace napříč všemi 4 aplikacemi.

\begin{lstlisting}[caption={~Předpis jmenného prostoru Network},label=list:network_namespace,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
namespace Network {
class Connection {
    Connection(std::shared_ptr<INetworkExecutor> executor, ...);
    template<typename T>
    void send(T a);
};
class Server {
    Server(int listening_port,
        std::shared_ptr<INetworkExecutor> connection_executor);
    void start();
    void stop();
};
class Client {
public:
    static std::shared_ptr<Connection> CONNECT(
        const std::string & ip_adress, int port,
        std::shared_ptr<INetworkExecutor> executor);
}; };
\end{lstlisting}


Poslední sdílenou částí je \verb/HealthMohitor/~\ref{list:healthmonitor}. Ten zprostředkovává data o vytíženosti. K dispozici jsou metody pro získání informací o CPU a paměti ram. Ve třídě je dále možné nastavit jméno a identifikátor aplikace. Tyto informace lze využít při výpisu. Identifikátor je také potřeba, pokud Klient chce odeslat data do Kamera. Předpokládá se, že Klient pošle dotaz na stav zařízení a tím zjistí identifikátor potřebný k adresování správné instance aplikace.


\begin{lstlisting}[caption={~Předpis třídy HealthMonitor},label=list:healthmonitor,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
class HealthMonitor
{
public:
    double cpu_usage();
    double mem_usage();
    HealthStruct* get_health();
    void set_data(std::string name, unsigned int app_id);
};
\end{lstlisting}



    \section{Úložiště}
    Pro snazší práci s ukládáním dat, bylo navrženo rozhraní, které má za cíl oddělit implementační detaily jednotlivých způsobů persistentního ukládání dat. Předpis rozhraní je ve výpisu~\ref{list:storage_interface}. Kromě samotných metod pro uložení a načtení byla přidána i metoda, která umožňuje úložiště prohledávat. Vybrán byl způsob dotazování na konkrétní den uložení. Používá se řetězec ve tvaru YYYY/MM/DD. Zvolená návratová hodnota je pole obsahující objekty typu \verb|DatalessBloock|. Struktura obsahuje ID na které je možné se dotázat a získat odpovídající data.

    Aktuální řešení obsahuje dvě různé implementace zmíněného rozhraní. Jedna varianta je \verb|Local Storage|, ta využívá k ukládání lokální disk. Druhou variantou je \verb|NetworkStorageClient| a přidružený \verb|NetworkStorageServer|. Tato varianta umožňuje ukládat data vzdáleně za použití navrženého síťového protokolu a také poskytuje data o vytíženosti. Pro lokální uložení byla zvolena následující hierarchie složek.
\begin{lstlisting}[caption={~Header struktura},label=list:storage_interface,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
class IStorage
{
public:
    virtual void store(DataBlock* data) = 0;
    virtual DataBlock* retrieve(BlockIdentifier identifier) = 0;
    virtual vector<DatalessBlock*> retrieve_by_date(string date) = 0;
};
\end{lstlisting}

	\dirtree{%
		.1 root.
		.2 2023.
		.3 3.
		.4 15.
		.5 block1.
		.3 4.
		.4 20.
		.5 block2.
		.5 block3.
		.5 block4.
		.4 21.
		.5 block5.
	}

    Aplikace Úložiště používá obě dvě implementace rozhraní. Server pro síťovou variantu přímo předává požadavky lokální implementaci. Vztah je zachycen na diagramu~\ref{fig:classdiagram-storage}.

    \begin{figure}[ht]\centering
        \includegraphics[width=1\textwidth]{img/storage-class-diagram.pdf}
         \caption[Diagram tříd Úložiště]{Diagram tříd Úložiště}\label{fig:classdiagram-storage}
    \end{figure}
    



    \section{Master}
    Master má za úkol agregovat \verb|DataFragment| a obsluhovat požadavky. Pro agregaci bylo navrženo jednoduché rozhraní \verb|IAgregationAlgorithm|~\ref{list:agregation_interface}. Návrh spočívá ve spuštění agregace v samostatném vláknu. Při inicializaci jsou na vstupu dodány dvě fronty, které jsou navrženy tak,  aby podporovali synchronní přístup. Jedna fronta složí k předávání \verb|DataFragment| a druhá přebírá zpracované \verb|DataBlock|. Metoda \verb|run| je blokující a slouží k spuštění agregačního algoritmu. Metodou \verb|close| se pak oznámí ukončení. Po zavolání \verb|close| jsou do front poslány prázdné objekty, aby se čekající vlákna odblokovala a mohla se ukončit. Na obrázku ~\ref{fig:sequencediagram_agregation} je znázorněn sekvenční diagram při použití 2 Kamera instancí. 

\begin{lstlisting}[caption={~Rozhraní pro agregaci},label=list:agregation_interface,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
class IAgregationAlgorithm
{
public:
    IAgregationAlgorithm(Queue<DataBlock*>* block_queue,
        Queue<DataFragment*>* fragment_queue)
    : m_block_queue(block_queue), m_fragment_queue(fragment_queue){}
    virtual void run() = 0;
    virtual void close() = 0;
protected:
    Queue<DataBlock*>* m_block_queue;
    Queue<DataFragment*>* m_fragment_queue;
};
\end{lstlisting}	
        
    \begin{figure}[ht]\centering
        \includegraphics[width=1\textwidth]{img/agregation}
         \caption[Sekvenční diagram agregace]{Sekvenční diagram komunikace během agregace.}\label{fig:sequencediagram_agregation}
    \end{figure}
    

    Pro zajištění obsluhy Kamera části jsou v Master nadefinované rozhraní \verb|IDeviceManager| a~\verb|ISourceDevice|. Implementace umožňuje připojit více nehomogenních zařízení a pracovat s~nimi pomocí jednotného rozhraní.  

    \begin{figure}[ht]\centering
        \includegraphics[width=1\textwidth]{img/device_manager_class_diagram.pdf}
         \caption[Třídní diagram pro správu zařízení]{Třídní diagram pro správu zařízení}\label{fig:classdiagram_device_management}
    \end{figure}


        
    \section{Kamera}
    Hlavní funkcionalitou části je získat zpracovaná data a odeslat je do Master. Hlavní problematikou je práce s kamerou a potřebné knihovny. Výběr jedné knihovny ovlivní flexibilitu řešení. Dále je potřeba umožnit algoritmu strojového vidění nastavovat parametry kamery a dohodnout formát pro výměnu obrazových dat. Pro tento postup je příliš málo informací. Proto je upuštěno od jakékoliv komunikace s kamerou.

    Master očekává \verb|DataFragment| jako vstup pro agregaci. Stejné řešení jde použít i zde. Pro algoritmus bude existovat pouze jediný kontrakt a to ten, že na konci každého cyklu musí vyplnit strukturu \verb|DataFragment| a umístit jí do fronty. Aplikace se pak postará o dopravení objektu do Master.

    Tento postup výrazně zjednodušuje řešení a nabízí vysokou flexibilitu. Nelze odhadnout, jak bude algoritmus kameru využívat, proto v tomto případě spadá zodpovědnost za obsluhu kamery na samotný algoritmus. Rozhraní je ukázáno na výpisu~\ref{list:algorithm_interface}.
    \footnote{Agregace a algoritmus nyní používají kompatibilní rozhraní. Pro řešení, která nepotřebují škálovatelnost na více strojů tak vzniká příležitost triviálně odstranit síťovou komunikaci a přiblížit se monolitickému řešení. Myšlenka může být dále rozvinuta využitím LocalStorage.}
\begin{lstlisting}[caption={~Předpis rozhraní pro algoritmus strojového vidění},label=list:algorithm_interface,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
class IAlgorithm
{
public:
    IAlgorithm(Queue<DataFragment*>& fragment_queue)
        : m_fragment_queue(fragment_queue) {}
    virtual void run() = 0;
    virtual void stop() = 0;
    virtual void process(CommandData* command_data) = 0;
protected:
    Queue<DataFragment*>& m_fragment_queue;
};
\end{lstlisting}	

    \section{Klient}
    Klient má za úkol zobrazovat data. Pro některá použití se hodí grafické rozhraní a pro jiná stačí konzole. Proto je Klient pojat jako rozhraní, které zabaluje volání síťového protokolu. Rozhraní je implementováno třídou \verb|ClientInterface|, viz kód~\ref{list:clientinterface}. Klient se připojuje na Master, který požadavky zpracovává. Před zavoláním metody \verb|send_command|, by se Klient měl ujistit o existenci příjemce, k tomu lze použít \verb|request_health_update|.
        
\begin{lstlisting}[caption={~Předpis třídy ClientInterface},label=list:clientinterface,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
class ClientInterface
{
public:
    std::vector<HealthStruct*> request_health_update();
    std::vector<DataBlock*> request_data_block(
            BlockIdentifier block_identifier);
    void send_command(std::string command, unsigned int receiver);
    std::vector<DatalessBlock*> block_query(std::string query);
private:
    Network::Connection connection;
};
\end{lstlisting}	        

\chapter{Testování}
    U testování musí být zajištěna replikovatelnost. Další problém je, že vision systém používá kamery, to zvyšuje nároky na přípravu testu. Řešení je navrženo tak, že vision systém je abstrahovaný na rozhraní \verb|IAlgorithm|. Je tedy možné jej nahradit libovolným algoritmem implementujícím dané rozhraní. Byl proto vytvořen pseudo případ použití pro vision systém, který lze otestovat lokálně a průběh testu lze replikovat.

    
    Jako experiment bylo zvoleno zpracování velkého množství obrázků, v obrázcích jsou náhodně rozmístěny zelené pixely. Úkolem je, tyto zelené pixely najít a zaznamenat, na jakém indexu se nacházejí. Formát obrázků je definovaný jako posloupnost RGB dat bez oddělovačů, kde se pro každý barevný kanál používá 1 bajt. Obrázky jsou pojmenovány unikáním číslem a data pro každý obrázek jsou rozděleny do N složek. Z každé složky smí číst pouze jedna instance Kamera. Pro nalezení všech zelených pixelů, je potřeba přečíst data ze všech složek a agregovat je. Obrázky jsou fixní velikosti známé předem. Při agregaci musí být index pixelu správně posunut podle toho, v jakém pořadí je daná část obrázku.

    Tento problém je zaměřen na otestování zpracování dat. Data budou přečtena z částí Klient a porovnána s daty získanými při generování obrázků. Pro otestování předávání funkčnosti předávání dat z Klient do Kamera se problém dále rozšíří o podmínku, že Kamera začne zpracovávat data až poté, co od Klient dostane povel \verb|start|. Testování výkonnosti a škálovatelnosti bude provedeno zvlášť po ověření funkčnosti řešení. 
    \section{Testovací implementace}
        Při návrhu datové reprezentace, byla uvedena možnost konkretizace datových struktur. Pro řešení zadaného problému je vhodné takto postupovat a proto je u struktur \verb|DataBlock| a \verb|DataFragment| změněn typ proměnné \verb|data| na \verb|vector<unsigned int>|\footnote{Změna datového typu nevyžadovala žádnou další změnu kódu, kromě upravení testovacích výpisů. Použitá knihovna pro serializaci změnu aplikovala v rámci síťové komunikace a při ukládání dat.}. Nový typ reprezentuje pole indexů zelených pixelů.


        Pro zpracování obrázků je použit algoritmus uvedený v~\ref{list:pseudo_algorithm}. Každá instance Kamera nyní dostane ve svém konfiguračním souboru cestu ke složce obsahující přidělené obrázky. Algoritmus je dále doplněn o proměnou \verb|can_start|. Ta je nastavena na \verb|false| a za použití aktivního čekání se čeká na zprávu od Klient, že se může začít.
\begin{lstlisting}[caption={~Pseudo algoritmus zpracování obrázku},label=list:pseudo_algorithm,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
for each file in folder
    DataFragment df {identifier = file.name}
    unsigned char R,G,B
    unsigned int iter = 0
    while !file.eof
        file >> R >> G >> B
        if R == 0 && G == 255 && B == 0
            df.data.add(iter)
        iter++
    fragment_queue.push(df)
\end{lstlisting}

        V Master byl přidán agregační algoritmus a upraven konfigurační soubor. Pro agregaci je využita datová struktura mapa. Pseudokód je uveden v ukázce~\ref{list:pseudo_agregation}. Hodnota N odpovídá počtu instancí Kamera a společně s \verb|PIXELS_PER_FILE| je součástí konfigurace.


\begin{lstlisting}[caption={~Pseudo algoritmus agregace},label=list:pseudo_agregation,captionpos=t,float,abovecaptionskip=-\medskipamount,belowcaptionskip=\medskipamount,language=C++]	
while run == true
    DataFragment df = fragment_queue.pop
    if fragment_map.contains(df.identifier) == false
        fragment_map.add(df.identifier, array)
    fragment_map[df.identifier ].add(df)
    if fragment_map[df.identifier].size != N
        continue
    DataBlock db {identifier = df.identifier}
    unsigned int i
	for each fragment in fragment_map[df.identifier]
	    for each index in df.data
	        db.data.push_back(index + i * PIXELS_PER_FILE)
	    ++i
    block_queue.push(db)
\end{lstlisting}

    Tímto je shrnuto přidání vision systému do navrženého řešení. Při řešení reálných problému se dá očekávat, že bude potřeba rozšířit konfigurační soubory o více proměnných. Všechny změny byly zcela izolované v rámci dané části řešení a  navržená rozhraní umožnila snadnou implementaci. I když neexistují žádné metriky a jedná se o subjektivní závěr, rozsah aplikovaných změn naznačuje možnost snadného rozšíření systému a splnění odpovídajícího nefunkčního požadavku.



    \section{Test funkčnosti}
    Řešení je testováno na operačním systému Windows. Vzhledem k použitým knihovnám by mělo být možné program zkompilovat na řadě dalších systémů a chování by mělo být stejné. Pro ověření funkčnosti je test rozdělen na dvě části. První test používá problém o rozsahu 2 složek a~3 souborů. Cílem je ověřit správnou funkčnost síťové komunikace a proto je Master spuštěn na jiném zařízení. Všechny ostatní částí komunikují s Master a proto každý požadavek musí jít přes síť a ne lokálně. Druhý test testuje větší instanci problému s 3 složkami a 10 soubory a~neobsahuje kontrolní výpisy síťové knihovny. Pozornost se klade na splnění funkčních požadavků. 
        \subsection{Síť}
        Na obrázku~\ref{fig:network_test_log} je vidět výstup testu sítě. Test proběhl v pořádku a všechna data byla správně předána mezi aplikacemi. Pro lepší vizualizaci je přiložena časová osa jednotlivých událostí. 
\begin{verbatim}
T0: Inicializace všech částí
T1: Klient žádá o data o stavu jednotlivých částí.
T2: Master přebírá požadavek.
T3-T6: Kamera 1, Kamera 2 a Uložiště odpoví Master.
T7: Master predá data Klientovi.
T8: Klient posílá příkaz start mířený na Kamera 1.
T9: Master předává zprávu do Kamera 1.
T9: Klient posílá příkaz start mířený na Kamera 2.
T10: Kamera 1 započíná činnost.
T10: Master předává zprávu do Kamera 2.
T10: Kamera 1 odesílá DataFragment.
T11: Kamera 2 započíná činnost.
T11: Master přijíma DataFragment z Kamera 1 a ukládá k agregaci.
T11: Kamera 2 odesílá DataFragment.
T12: Master přijíma DataFragment z Kamera 2 a ukládá k agregaci.
T13: Master agreguje data a posílá první DataBlock do Uložiště.
T14: Uložiště přijímá DataBlock a ukládá jej.
... Stejné operace pro druhý a třetí DataBlock.
T30: Klient žádá o data zpracována 2023/4/30.
T31: Master předává požadavek.
T32: Uložiště odpovídá.
T33: Master odpovídá.
T34: Klient zobrazí data.
T35: Klient se ptá na DataBlock 1.
... Následuje stejná posloupnost výměny dat. 
\end{verbatim}
        

    \begin{figure}[ht]\centering
        \includegraphics[width=1\textwidth]{img/network_test.png}
         \caption[Log testování síťové komunikace]{Log testování síťové komunikace.}\label{fig:network_test_log}
    \end{figure}
        
        \subsection{Splnění požadavků}
        Pro splnění požadavků je potřeba otestovat následující funkce:
        \begin{itemize}
            \item Zpracování a uložení dat.
            \item Zpřístupnění dat.
            \item Propagace dat ke kamerám.
            \item Informace o vytíženosti.
        \end{itemize}
        V Předešlém testu již bylo potvrzeno několik funkcí. Lze propagovat data ke kamerám, když je potřeba poslat start příkaz. Uložení dat bylo také ukázáno. Lze se tedy soustředit na správnost zpracovaných dat a měření vytíženosti. 

        Nyní je potřeba přímo pracovat s testovacími daty. Vygenerovaná data jsou zobrazena v~ob\-rá\-zku~\ref{fig:function_test_data}. Dále byl pro účel testu upraven algoritmus pro zpracování souborů tak, aby při každém zpracování aktivně čekal 2 sekundy.
        

        
    \begin{figure}[ht]\centering
        \includegraphics[width=0.9\textwidth]{img/function_test_data.png}
         \caption[Výpis z generátoru testovacích dat]{Výpis z generátoru testovacích dat.}\label{fig:function_test_data}
    \end{figure}

        Na obrázku~\ref{fig:function_test_result} jsou výsledky testu. Data, která řešení vyprodukovalo jsou shodná s daty nagenerovanými. Byly nalezeny 4 soubory se zelenými pixely. Výpis byl formátován pro lepší čitelnost.


    \begin{figure}[ht]\centering
        \includegraphics[width=0.7\textwidth]{img/function_test_result.png}
         \caption[Výsledek testu]{Výsledek testu na přiložených datech.}\label{fig:function_test_result}
    \end{figure}

        Během běhu testu byl dále spuštěn příkaz pro získání dat o vytíženosti~\ref{fig:function_test_health}. Instance Kamera zde ukazují hodnotu 4,16 \%, mezitím co Master a Úložiště 0 \%. Hodnota okolo 4 \% dává smysl, jelikož test probíhal na stroji s 24 logickými jádry. 4 \% odpovídá plnému využití jednoho jádra aktivním čekáním. Tento údaj se ale může na první pohled jevit jako mylný a vzniká otázka zda by nebylo lepší použít jiné metriky. 


    \begin{figure}[ht]\centering
        \includegraphics[width=0.35\textwidth]{img/function_test_health.png}
         \caption[Informace o vytíženosti]{Informace o vytíženosti.}\label{fig:function_test_health}
    \end{figure}
        Tímto je předvedena funkčnost celého řešení. Všechny funkční požadavky byly splněny. 
    
    \section{Test škálovatelnosti}
        Pro otestování škálovatelnosti byly vybrány dvě metriky. Počet kamer a velikost dat. Pro každý případ bude pozměněna implementace algoritmu a agregace.
        
        \subsection{Počet kamer}
        Pro měření je použit algoritmus, který každých 20 ms vygeneruje 400 bajtů dat. Cílem je zatížit systém a zjistit jak se chová vzhledem k rostoucímu počtu připojených zařízení. První návrh testu počítal s použitím vytíženosti procesoru jako metriky. Takto vypadá vytížení při použití 30 Kamer~\ref{fig:cpu_usage}. Celkové vytížení se pohybovalo okolo 14\% a z toho byla nezanedbatelná část procesy na pozadí. Při pokusu dále zvýšit počet zařízení bylo limitujícím faktorem přepínání kontextu procesoru.

    \begin{figure}[ht]\centering
        \includegraphics[width=0.8\textwidth]{img/cpu_usage.png}
         \caption[Vytížení procesoru]{Obrázek zobrazuje 24 logických a jejich vytížení při použití 30 Kamer.}\label{fig:cpu_usage}
    \end{figure}

        Byla zvolena metrika odpovídající době potřebné pro zpracování 1000 bloků. Toto měření probíhá na straně Úložiště. Tabulka~\ref{table:camer_count} obsahuje naměřená data.
\begin{table}
\caption[Výsledky měření škálovatelnosti vzhledem k počtu kamer]{~Výsledky měření škálovatelnosti vzhledem k počtu kamer.}\label{table:camer_count}
\begin{center}
\begin{tabular}{|c|c c c c|} 
 \hline
 Identifikátor bloku & 5 Kamer & 10 Kamer & 20 Kamer & 30 Kamer \\
 \hline
 1000 & 26,8 s & 27,1 s & 27,9 s & 29,1 s \\ 
 \hline
 2000 & 47,3 s & 47,8 s & 49,1 s & 50,9 s\\ 
 \hline
 3000 & 67,9 s & 68,7 s & 70,2 s & 72,4 s\\ 
 \hline
 4000 & 88,5 s & 89,5 s & 91,7 s & 94,3 s\\ 
 \hline
\end{tabular}
\end{center}
\end{table}

        Drobná prodleva u první měřené hodnoty je čas, který systém má na svůj start. Hodnoty odpovídají rozdílu 20~s s přidanou režií. Režie je nejvyšší při použití 30~Kamer a to okolo 2~s. Hodnota 30~kamer je také shodná s horním limitem zjištěním v rešerši. Rostoucí prodleva může být způsobena přepínáním kontextu. Podle dat z testu lze očekávat, že řešení se bude škálovat dobře podle počtu připojených vision systémů.

        \subsection{Velikost dat}
         Velikost dat se měří, aby se zjistilo, jak systém reaguje na rostoucí velikost zpracovaných dat. Test by měl především zatížit implementovaný protokol a metrikou je potřebný čas pro agregaci 10 bloků při použití 10 kamer. Výsledky měření jsou zachyceny v tabulce~\ref{table:data_size}. 
\begin{table}
\caption[Výsledky měření škálovatelnosti vzhledem k velikosti dat]{~Výsledky měření škálovatelnosti vzhledem k velikosti dat.}\label{table:data_size}
\begin{center}
\begin{tabular}{|c| c c c|} 
 \hline
 Identifikátor bloku & 1 MB & 10 MB & 100 MB \\
 \hline
 1 & 5.7 s & 7,3 s & 17,2 s \\ 
 \hline
 2 & 5,7 s & 7,6 s & 20,5 s \\ 
 \hline
 3 & 5,8 s & 7,9 s & 23,2 s \\ 
 \hline
 4 & 5,8 s & 8,1 s & 25,7 s \\ 
 \hline
 5 & 5,8 s & 8,3 s & 27,8 s \\ 
 \hline
 6 & 5,9 s & 8,6 s & 30,4 s\\ 
 \hline
 7 & 5,9 s & 8,8 s & 32,9 s\\ 
 \hline
 8 & 5,9 s & 9,0 s & 35,2 s\\ 
 \hline
 9 & 5,9 s & 9,2 s & 37,3 s\\ 
 \hline
 10 & 6,0 s & 9,5 s & 39,7 s\\
 \hline
\end{tabular}
\end{center}
\end{table}


        V naměřených datech a při pozorování chování systému se vyskytl trend, že nejdříve jsou všechna data odeslána do Master a až poté jsou posílána do Úložiště. Ve variantě s 100 MB je potřeba přes síť přenést celkem 20 GB dat. Kamera zvládá data odeslat výrazně rychleji než je Master zvládá přijmout, a to by mohlo způsobit, že i když je již připraven prvním blok, tak kvůli plánování se dostane až na konec fronty a je odeslán poté, co jsou přečteny všechny příchozí data z Kamera.

        Lze říct, že řešení podporuje práci s velkými daty. Jestli je rychlost dostačující, záleží v jaké doméně je nasazeno. Test lze interpretovat tak, že systém zvládne postupně zpracovat objemná data, pokud je dostatečný čas mezi jejich příchodem.


        
\chapter{Diskuze}
    Na závěr praktické části je k dispozici seznam možných změn a vylepšení, kterými lze práci dále rozšířit.
    \begin{description}
        \item[Konkretizace řešení] Řešení je postavené na velkém množství abstrakce. Abstrakci lze v~mnoha případech lehce odstranit nahrazením jednotlivých rozhraní konkrétními implementacemi napříč systémem. Změna je vhodná, pokud je žádoucí mít úzce specializované řešení.
        \item [Kontejnerizace] Pro využití v cloudových řešeních je vhodné jednotlivé části zabalit do kontejnerů pro použití v prostředích jako je například Docker.
        \item [Pokročilé monitorování] Na trhu existují sofistikované řešení, která lze použít místo postupu implementovaného v této práci. Řešení poskytují vlastní grafické rozhraní, které zahrnuje informace o stavu jednotlivých částí systému.
        \item [Implementace jiného úložiště] Úložiště lze nahradit za databázové úložiště či cloudové. Pokud je potřeba data uchovávat mimořádně dlouho, lze zvážit uložení na magnetické pásky, kde neověřené zdroje uvádějí životnost 10 až 20 let.
        \item [Implementace jiného síťového protokolu] Protokol TCP nemusí být nejvýhodnějším řeše\-ním pro každé zadání. Pro lokální sítě s nízkou ztrátovostí a nutností přenášet extrémně objemná data by mohl například protokol UDP poskytnout vyšší výkon.
        \item [Dynamické načítání knihoven] C++ umožňuje načtení knihovny při startu programu ale i~později za běhu. Algoritmy pro strojové vidění nejsou součástí práce a je potřeba jednotlivé části celé zkompilovat, pokud dojde ke změnám nejen v rozhraních. Algoritmy je možné zabalit do knihovny a tu načíst buď při startu, nebo jít o kus dále a umožnit provádět změny algoritmu za běhu.
        \item [Vytvoření rozhraní pro jiné programovací jazyky] Aby bylo možné použít i jazyky jako je Python nebo Java pro implementaci algoritmů, je potřeba zpřístupnit důležitá rozhraní a~přidat logiku pro obsluhu virtuálních strojů, které uvedené jazyky používají.
        \item [Tvorba metrik] Pokud je každý vytvořený produkt zaznamenán pomocí systému, lze vytvořit aplikaci, která se připojí na úložiště a bude vytvářet metriky jako je počet vyrobených kusů za časový úsek, či chybovost výrobního procesu. 
    \end{description}