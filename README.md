# Efektivní komunikace více vision systémů
Repozítář pro diplomovou práci Efektivní komunikace více vision systémů.
Vypracoval Bc. Jindřich Kuzma pod vedením Ing. Jakuba Nováka.
## Obsah
```markdown
├── exe                  adresář se spustitelnou formou implementace
│   └── README.md        návod na spuštění
├── src
│   ├── impl             zdrojové kódy implementace
│   └── thesis           zdrojová forma práce ve formátu LaTeX
├── text   
│   └── thesis.pdf       text práce ve formátu PDF           
└── README.md
```