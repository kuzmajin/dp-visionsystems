#connection details
master_port=900
master_ip=127.0.0.1
#algorithm support "green" or "test"
app_id=1
algorithm=green
#find green algorithm
algorithm_data_folder=./test_data/1
#test algorithm
generated_data_size=100
generated_iterations=30000000
sleep_ms_between_generating=20
