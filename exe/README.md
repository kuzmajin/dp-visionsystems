# Návod na spuštění
Řešení je spustitelné na platformě Windows. Otestována byla verze Windows 10 64-bit.

## Popis
Zkompilované aplikace jsem k dispozici ve složce exe. Složka configs obsahuje konfigurační soubory potřebné pro běh aplikací. Scripts obsahuje další skripty použité k ulehčení spuštění. Ve složce test_data jsou předgenerována data, která jsou použita v defaultní konfiguraci. Storage se používá k ukládání dat, je vhodné obsah složky smazat mezi spuštěními. 

## Skripty

K dispozici jsou 3 skripty určené k usnadnění práce s aplikacemi. 

- run - Spustí celé řešení za použití konfiguračních souborů ze složky configs.
- generate_data - Spustí generátor testovacích dat pro hledání zeleného pixelu.
- generate_camera_configs - Předgeneruje konfiguraci pro N instancí Kamera.

## Konfigurace
Kromě nastavení koncových bodů pro síťovou komunikaci je potřeba, aby Master aplikace věděla kolik kamer poběží, tato hodnota se předává v konfiguraci hodnotou cameras_count. Dále je potřeba aby Master a Kamera měli nastavenou stejnou hodnotu algoritmu. 

## Spuštění
Jakmile jsou nastaveny konfigurace je možné vše spustit skriptem run.
```
run [počet kamer]
```
Příkaz postupně spustí jednotlivé aplikace. Pro možnost otestování je připravena konfigurace pro 3 kamery.
```
run 3
```
Pokud je použit algoritmus pro hledání zelených pixelů, je potřeba rozeslat start příkazy.

## Obsluha
Aplikace Client.exe přijímá následující vstupy od uživatele.
```
health - vypíše vytíženost jednotlivých zařízení
start [id kamery] - pošle start příkaz do zvolené kamery
YYYY/MM/DD - vypíše id bloků uložených k danému datu, například 2023/4/20
[id bloku] - vypíše informace o daném bloku
```