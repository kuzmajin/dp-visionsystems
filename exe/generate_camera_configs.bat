del /s camera*.txt


FOR /L %%i IN (1,1,%1) DO (
  echo #connection details>.\configs\camera%%i.txt
  echo master_port=900>>.\configs\camera%%i.txt
  echo master_ip=127.0.0.1>>.\configs\camera%%i.txt
  echo #algorithm support "green" or "test">>.\configs\camera%%i.txt
  echo app_id=%%i>>.\configs\camera%%i.txt
  echo algorithm=green>>.\configs\camera%%i.txt
  echo #find green algorithm>>.\configs\camera%%i.txt
  echo algorithm_data_folder=./test_data/%%i>>.\configs\camera%%i.txt
  echo #test algorithm>>.\configs\camera%%i.txt
  echo generated_data_size=100>>.\configs\camera%%i.txt
  echo generated_iterations=30000000>>.\configs\camera%%i.txt
  echo sleep_ms_between_generating=20>>.\configs\camera%%i.txt
)
pause